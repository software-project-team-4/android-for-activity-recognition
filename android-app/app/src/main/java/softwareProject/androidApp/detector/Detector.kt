package softwareProject.androidApp.detector

import android.util.Log
import softwareProject.androidApp.internalDatabase.RawData
import softwareProject.androidApp.internalDatabase.DbWorkerThread
import softwareProject.androidApp.internalDatabase.EventProbabilities
import softwareProject.androidApp.internalDatabase.SensorDatabase

class Detector {

    /**
     * The maximum length of the acceleration vector for the phone to be considered in free fall
     */
    private val fallThreshold = 1.0

    /**
     * Iterate over given sequence of [[AccDataPoint]] and store representing a free fall to database
     *
     * @param data a set of accelerator sensor data
     */
    fun processFalls(data: MutableList<RawData>, db: SensorDatabase?, thread: DbWorkerThread) {
        val falls = findFalls(data)

        if (falls.isNotEmpty()) {
            Log.i("info", "Inserting ${falls.size} falls to database\"")

            val task = Runnable {
                for (d in falls) {
                    db?.eventDAO()?.insert(d)
                }
            }
            thread.postTask(task)
        }
    }

    /**
     * Find the data points that specify a fall
     *
     * @param data a sequence of [[AccDataPoint]]
     * @return a sequence of datapoints at which the sensor was found to be in free fall
     */
    fun findFalls(data: MutableList<RawData>): MutableList<EventProbabilities> {
        val result = mutableListOf<EventProbabilities>()

        for (p in data) {
            val timeStamp = p.timestamp
            val x = p.acc_x.toDouble()
            val y = p.acc_y.toDouble()
            val z = p.acc_z.toDouble()
            val length = Math.sqrt(x * x + y * y + z * z)
            if (length < fallThreshold) {
                result += EventProbabilities(timeStamp, timeStamp, "fall", "0")
            }
        }
        return result
    }
}