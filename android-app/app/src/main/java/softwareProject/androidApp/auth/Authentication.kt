package softwareProject.androidApp.auth

import android.app.Activity
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient

object Authentication {

    /**
     * The client id of the backend for google api
     */
    private const val CLIENT_ID = "1085465719221-01su2ht731ug6pnqmekj13h0epi7e8ht.apps.googleusercontent.com"

    /**
     * A google sing in options object for building sing in clients
     */
    private val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestIdToken(CLIENT_ID)
        .requestEmail()
        .build()

    /**
     * Use the signed in users id token
     *
     * If necessary attempts to re authenticate silently
     * If that fails starts the manual login activity with given request code
     *
     * Note that the callbacks will get called from another thread so do not perform any UI actions on
     * the callbacks directly
     *
     * @param activity the activity requiring the token
     * @param callback the function that uses the token
     * @param requestCode the request code that will be used for creating the login activity if necessary
     * @param error an optional callback if you want to instantly know that the silent login has failed
     */
    fun withToken(activity: Activity, callback: (String) -> Unit, requestCode: Int, error: () -> Unit = {}) {
        Thread(Runnable {
            val apiClient = GoogleApiClient.Builder(activity).addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()

            // ensure that the client is connected
            if (!apiClient.isConnected)
                apiClient.blockingConnect()

            // attempt to silently sign in
            val opr = Auth.GoogleSignInApi.silentSignIn(apiClient)
            opr.setResultCallback { result ->
                if (result.isSuccess) {
                    result.signInAccount?.idToken?.let { callback(it) }
                } else { // manual sign in in necessary
                    // let the caller know instantly that the operation has failed
                    error()
                    // sign in manually
                    startManualSignIn(activity, requestCode)
                }
            }
        }).start()
    }

    /**
     * Open the manual sign in interface for the user to pick an account
     * If an account has already been selected before it will be used
     *
     * @param activity the activity requiring sign in
     * @param requestCode the request code of the sign in for identifying the sign in on result
     */
    private fun startManualSignIn(activity: Activity, requestCode: Int) {
        val signInClient = GoogleSignIn.getClient(activity, gso)
        activity.startActivityForResult(signInClient.signInIntent, requestCode)
    }
}