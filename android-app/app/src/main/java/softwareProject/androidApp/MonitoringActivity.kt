package softwareProject.androidApp

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.monitoring_layout.*

class MonitoringActivity : AppCompatActivity(), SensorEventListener {

    private lateinit var mSensorManager: SensorManager
    private var mAcc: Sensor? = null
    private var mGyro: Sensor? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.monitoring_layout)
        window.decorView.apply {
            systemUiVisibility =
                View.SYSTEM_UI_FLAG_IMMERSIVE or View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }
        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mAcc = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        mGyro = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
        mSensorManager.registerListener(this, mGyro, SensorManager.SENSOR_DELAY_NORMAL)
        mSensorManager.registerListener(this, mAcc, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }

    override fun onSensorChanged(event: SensorEvent) {
        val sensor = event.sensor

        val x = event.values[0]
        val y = event.values[1]
        val z = event.values[2]
        if (sensor.type == Sensor.TYPE_ACCELEROMETER) {
            acc_x.text = x.toString().take(6)
            acc_y.text = y.toString().take(6)
            acc_z.text = z.toString().take(6)
        } else {
            gyr_x.text = x.toString().take(6)
            gyr_y.text = y.toString().take(6)
            gyr_z.text = z.toString().take(6)
        }
    }
}