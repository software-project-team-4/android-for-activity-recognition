package softwareProject.androidApp

import org.json.JSONArray
import org.json.JSONObject
import softwareProject.androidApp.internalDatabase.SensorDatabase

object ParseJson {

    private val maxSize = 500
    var sensorDb: SensorDatabase? = null

    fun fetchDataToJson(): Triple<JSONArray, Long, Boolean> {
        val data = sensorDb?.dataPointDAO()?.getAllDataPoints()
        val dataCollector = JSONArray()
        var collectedAll = true
        val size = data!!.size
        if (size > maxSize)
            collectedAll = false
        var max = 0L
        val count = Math.min(size, maxSize)
        var i = 0
        if (size != null) {
            while (i < count) {
                val currentValue = data[(size - 1) - i]
                max = Math.max(max, currentValue.timestamp)
                val jsonObject = JSONObject()
                jsonObject.put("timestamp", currentValue.timestamp)
                jsonObject.put("acc_x", currentValue.acc_x)
                jsonObject.put("acc_y", currentValue.acc_y)
                jsonObject.put("acc_z", currentValue.acc_z)
                jsonObject.put("gyr_x", currentValue.gyro_x)
                jsonObject.put("gyr_y", currentValue.gyro_y)
                jsonObject.put("gyr_z", currentValue.gyro_z)
                dataCollector.put(jsonObject)
                i += 1
            }
        }
        return Triple(dataCollector, max, collectedAll)
    }

    fun fetchEventDataToJson(): Triple<JSONArray, Long, Boolean> {
        val eventData = sensorDb?.eventDAO()?.getAllDataPoints()
        val size = eventData!!.size
        val collector = JSONArray()
        var max = 0L
        var collectedAll = true
        val count = Math.min(size, maxSize)
        if (size > maxSize)
            collectedAll = false
        var i = 0
        if (size != null) {
            while (i < count) {
                val currentValue = eventData[(size - 1) - i]
                val jsonObject = JSONObject()
                max = Math.max(max, currentValue.timestamp_start)
                jsonObject.put("timestamp_start", currentValue.timestamp_start)
                jsonObject.put("timestamp_end", currentValue.timestamp_end)
                jsonObject.put("model_name", currentValue.model_name)
                jsonObject.put("probabilities", currentValue.probabilities)
                collector.put(jsonObject)
                i += 1
            }
        }
        return Triple(collector, max, collectedAll)
    }

    fun fetchAnnotationDataToJson(): Triple<JSONArray, Long, Boolean> {
        val annotationData = sensorDb?.annotationDAO()?.getAllDataPoints()
        val size = annotationData!!.size
        val collector = JSONArray()
        var max = 0L
        var collectedAll = true
        val count = Math.min(size, maxSize)
        if (size > maxSize)
            collectedAll = false
        var i = 0
        if (size != null) {
            while (i < count) {
                val currentValue = annotationData[(size - 1) - i]
                max = Math.max(max, currentValue.timestamp_start)
                val jsonObject = JSONObject()
                jsonObject.put("timestamp_start", currentValue.timestamp_start)
                jsonObject.put("timestamp_end", currentValue.timestamp_end)
                jsonObject.put("event_name", currentValue.event_name)
                collector.put(jsonObject)
                i += 1
            }
        }
        return Triple(collector, max, collectedAll)
    }

    fun genEventJson(): Triple<Pair<JSONObject, Boolean>, Long, String> {
        val finalObject = JSONObject()
        val data = fetchEventDataToJson()
        val eventData = data.first
        val timestamp = data.second
        val collectedAll = data.third
        finalObject.put("data", eventData)
        return Triple(Pair(finalObject, collectedAll), timestamp, "eventData")
    }

    fun genAnnotationJson(): Triple<Pair<JSONObject, Boolean>, Long, String> {
        val finalObject = JSONObject()
        val data = fetchAnnotationDataToJson()
        val annotationData = data.first
        val timestamp = data.second
        val collectedAll = data.third
        finalObject.put("data", annotationData)
        return Triple(Pair(finalObject, collectedAll), timestamp, "annotationData")
    }

    // Future version where the json file is constructed of ID, Seq[Acc_data], Seq[Gyro_ Data]
    fun genRawJson(): Triple<Pair<JSONObject, Boolean>, Long, String> {
        val finalObject = JSONObject()
        val acc = fetchDataToJson()
        val accData = acc.first
        val timestamp = acc.second
        val collectedAll = acc.third
        finalObject.put("data", accData)
        return Triple(Pair(finalObject, collectedAll), timestamp, "insertData")
    }
    // TODO: Remove to here
    // Future version where the json file is constructed of ID, Seq[Acc_data], Seq[Gyro_ Data]
    fun genJsonFromDB(db: String): Triple<Pair<JSONObject, Boolean>, Long, String> {
        when (db) {
            "event" -> return genEventJson()
            "annotation" -> return genAnnotationJson()
            else -> return genRawJson()
        }
    }
}
