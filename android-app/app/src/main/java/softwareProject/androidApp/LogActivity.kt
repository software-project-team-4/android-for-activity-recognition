package softwareProject.androidApp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.log_screen.*
import softwareProject.androidApp.internalDatabase.SensorDatabase
import java.text.SimpleDateFormat
import java.util.Date

open class LogActivity : AppCompatActivity() {

    val sensorDB: SensorDatabase = SensorDatabase.database()

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.log_screen)
        showAnnotationLog()
        window.decorView.apply {
            systemUiVisibility =
                View.SYSTEM_UI_FLAG_IMMERSIVE or View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }
    }

    fun toAnnotationActivity(@Suppress("UNUSED_PARAMETER") view: View) {
        finish()
        overridePendingTransition(R.anim.slide_right2, R.anim.slide_right)
    }

    fun toSettingsActivity(@Suppress("UNUSED_PARAMETER") view: View) {
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
        finish()
        overridePendingTransition(R.anim.slide_right2, R.anim.slide_right)
    }

    fun showAnnotationLog() {
        sensorDB.getEventAnnotationPairs(8) { data ->
            runOnUiThread {
                val sdf: SimpleDateFormat = SimpleDateFormat("HH:mm")
                val listTextID = arrayOf(
                    annotation0,
                    annotation1,
                    annotation2,
                    annotation3,
                    annotation4,
                    annotation5,
                    annotation6,
                    annotation7
                )
                val listStID = arrayOf(ST0, ST1, ST2, ST3, ST4, ST5, ST6, ST7)
                val listEtID = arrayOf(ET0, ET1, ET2, ET3, ET4, ET5, ET6, ET7)
                val size = data.size
                var i = 0
                while (i < size) {
                    listTextID[i].text = data[i].annotation_name
                    listStID[i].text = sdf.format(Date(data[i].timestamp))
                    listEtID[i].text = data[i].event_name
                    i += 1
                }
            }
        }
    }

    /*fun showAnnotationLog(@Suppress("UNUSED_PARAMETER") view: View) {
        sensorDB.getAmountAnnPoints(8) { annotationData ->
            runOnUiThread {
                val sdf: SimpleDateFormat = SimpleDateFormat("HH:mm")
                val listTextID = arrayOf(
                    annotation0,
                    annotation1,
                    annotation2,
                    annotation3,
                    annotation4,
                    annotation5,
                    annotation6,
                    annotation7
                )
                val listStID = arrayOf(ST0, ST1, ST2, ST3, ST4, ST5, ST6, ST7)
                val listEtID = arrayOf(ET0, ET1, ET2, ET3, ET4, ET5, ET6, ET7)
                val size = annotationData.size
                var i = 0
                while (i < size) {
                    val annotation = annotationData[i].event_name
                    val startTime = annotationData[i].timestamp_start
                    val endTime = annotationData[i].timestamp_end
                    listTextID[i].text = annotation
                    listStID[i].text = sdf.format(Date(startTime))
                    listEtID[i].text = sdf.format(Date(endTime))
                    i += 1
                }
            }
        }
    }*/
}