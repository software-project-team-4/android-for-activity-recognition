package softwareProject.androidApp.internalDatabase

import android.os.Handler
import android.os.HandlerThread

class DbWorkerThread(threadName: String) : HandlerThread(threadName) {

    private val mWorkerHandler: Handler by lazy {
        Handler(looper)
    }

    /**
     * Gives the task to a separate thread, which completes it
     *
     * @param runnable a task that is given to the mWorkerThread
     */
    fun postTask(task: Runnable) {
        mWorkerHandler.post(task)
    }
}