package softwareProject.androidApp.internalDatabase

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

/**
 * Base DAO class that holds shared database methods for data point classes
 */

@Dao
interface BaseDAO<T> {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(obj: T)
}

@Dao
interface RawDataDAO : BaseDAO<RawData> {

    @Query("SELECT * FROM data_point ORDER BY 1 DESC;")
    fun getAllDataPoints(): List<RawData>

    @Query("SELECT * FROM (SELECT * FROM data_point ORDER BY 1 ASC LIMIT :amount) ORDER BY 1 DESC")
    fun getAmountDataPoints(amount: Int): List<RawData>

    @Query("DELETE FROM data_point")
    fun deleteAll()

    @Query("DELETE FROM data_point WHERE timestamp <= :last_timestamp ")
    fun deleteUntilDataPoint(last_timestamp: Long): Int

    @Query("SELECT * FROM (SELECT * FROM data_point WHERE timestamp > :eval_timestamp ORDER BY 1 ASC LIMIT :amount)")
    fun getAmountEvalDataPoints(amount: Int, eval_timestamp: Long): List<RawData>

    @Query("SELECT count(*) FROM data_point")
    fun getPointsCount(): Int
}

/**
 * Methods for accessing event data points in the database
 */
@Dao
interface EventDAO : BaseDAO<EventProbabilities> {

    @Query("SELECT * FROM event_probabilities ORDER BY 1 DESC;")
    fun getAllDataPoints(): List<EventProbabilities>

    @Query("SELECT * FROM (SELECT * FROM event_probabilities ORDER BY 1 ASC LIMIT :amount) ORDER BY 1 DESC")
    fun getAmountDataPoints(amount: Int): List<EventProbabilities>

    @Query("DELETE FROM event_probabilities")
    fun deleteAll()

    @Query("DELETE FROM event_probabilities WHERE timestamp_start <= :last_timestamp ")
    fun deleteUntilDataPoint(last_timestamp: Long): Int

    @Query("SELECT count(*) FROM event_probabilities")
    fun getPointsCount(): Int
}

/**
 * Methods for accessing event data points in the database
 */
@Dao
interface AnnotationDAO : BaseDAO<AnnotationDataPoint> {

    @Query("SELECT * FROM annotation_data ORDER BY 1 DESC;")
    fun getAllDataPoints(): List<AnnotationDataPoint>

    @Query("SELECT * FROM (SELECT * FROM annotation_data ORDER BY 1 DESC LIMIT :amount) ORDER BY 1 DESC")
    fun getAmountDataPoints(amount: Int): List<AnnotationDataPoint>

    @Query("DELETE FROM annotation_data")
    fun deleteAll()

    @Query("DELETE FROM annotation_data WHERE timestamp_start <= :last_timestamp ")
    fun deleteUntilDataPoint(last_timestamp: Long): Int

    @Query("DELETE FROM annotation_data WHERE timestamp_start in (SELECT timestamp_start FROM annotation_data WHERE timestamp_end <= :last_timestamp ORDER BY 1 ASC LIMIT :amount)")
    fun deleteAmountDataPoints(last_timestamp: Long, amount: Int): Int

    @Query("SELECT count(*) FROM annotation_data")
    fun getPointsCount(): Int
}

@Dao
interface AnnoEventDao : BaseDAO<AnnotationAndEvent> {

    @Query("SELECT * FROM (SELECT * FROM annotation_event ORDER BY 1 DESC LIMIT :amount) ORDER BY 1 DESC")
    fun getMatchingPairs(amount: Int): List<AnnotationAndEvent>

    @Query("SELECT count(*) FROM annotation_event")
    fun getPairAmount(): Int

    @Query("DELETE FROM annotation_event WHERE timestamp in (SELECT timestamp FROM annotation_event ORDER BY 1 ASC LIMIT :amount)")
    fun deleteAmountPairs(amount: Int): Int
}
