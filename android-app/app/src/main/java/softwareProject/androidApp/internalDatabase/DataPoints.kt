package softwareProject.androidApp.internalDatabase

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "data_point")
data class RawData(
    @PrimaryKey var timestamp: Long,
    @ColumnInfo(name = "acc_x") val acc_x: Float,
    @ColumnInfo(name = "acc_y") val acc_y: Float,
    @ColumnInfo(name = "acc_z") val acc_z: Float,
    @ColumnInfo(name = "gyr_x") val gyro_x: Float,
    @ColumnInfo(name = "gyr_y") val gyro_y: Float,
    @ColumnInfo(name = "gyr_z") val gyro_z: Float
)

/**
 * A class for holding events probabilities
 */
@Entity(tableName = "event_probabilities")
data class EventProbabilities(
    @PrimaryKey var timestamp_start: Long,
    @ColumnInfo(name = "timestamp_end") val timestamp_end: Long,
    @ColumnInfo(name = "model_name") val model_name: String,
    @ColumnInfo(name = "probabilities") val probabilities: String
)

/**
 * A class for holding annotations
 */
@Entity(tableName = "annotation_data")
data class AnnotationDataPoint(
    @PrimaryKey var timestamp_start: Long,
    @ColumnInfo(name = "timestamp_end") val timestamp_end: Long,
    @ColumnInfo(name = "event_name") val event_name: String
)

@Entity(tableName = "annotation_event")
data class AnnotationAndEvent(
    @PrimaryKey var timestamp: Long,
    @ColumnInfo(name = "annotation_name") val annotation_name: String,
    @ColumnInfo(name = "event_name") val event_name: String
)

data class AccDataPoint(
    var timestamp: Long,
    val acc_x: Float,
    val acc_y: Float,
    val acc_z: Float
)

data class GyrDataPoint(
    var timestamp: Long,
    val gyr_x: Float,
    val gyr_y: Float,
    val gyr_z: Float
)
