package softwareProject.androidApp.internalDatabase

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import android.database.sqlite.SQLiteFullException

@Database(
    entities = arrayOf(
        RawData::class, EventProbabilities::class, AnnotationDataPoint::class, AnnotationAndEvent::class
    ), version = 2
)

abstract class SensorDatabase : RoomDatabase() {
    abstract fun dataPointDAO(): RawDataDAO
    abstract fun eventDAO(): EventDAO
    abstract fun annotationDAO(): AnnotationDAO
    abstract fun annoEventDAO(): AnnoEventDao

    companion object {

        /**
         * The sensor database instance
         */
        private var _database: SensorDatabase? = null

        /**
         * The private worker thread instance
         */
        private var _workerThread: DbWorkerThread? = null

        /**
         * Get the current database instance
         */
        fun database(): SensorDatabase {
            return if (_database != null) _database!! else throw IllegalStateException("Database hasn't been initialized")
        }

        /**
         * Get the worker thread for performing database actions
         */
        fun workerThread(): DbWorkerThread {
            return if (_workerThread != null) _workerThread!! else throw IllegalStateException("Database hasn't been initialized")
        }

        /**
         * A function that initializes the database
         *
         * @param context the application context that the database is ran with
         * @param test whether or not the test database should be used
         */
        fun init(context: Context, test: Boolean = false) {
            _database = if (test) {
                Room.inMemoryDatabaseBuilder(
                    context.applicationContext, SensorDatabase::class.java
                ).allowMainThreadQueries().build()
            } else {
                Room.databaseBuilder(context.applicationContext, SensorDatabase::class.java, "sensor.db").build()
            }
            _workerThread = DbWorkerThread("dbWorkerThread")
            _workerThread?.start()
        }
    }

    /**
     * Insert a accelerator data point to the database. Delete already evaluated raw data if table limit exceeded or database full.
     *
     * @param dataPoint an accelerator data point, see DataPoints.kt file
     */
    fun insertDataPoint(dataPoint: RawData) {
        val insert_task = Runnable {
            _database?.dataPointDAO()?.insert(dataPoint)
            DatabaseVariables.totalDataPoints += 1
        }
        if (DatabaseVariables.totalDataPoints < DatabaseVariables.maxRowsInTable) {
            try {
                _workerThread?.postTask(insert_task)
            } catch (e: SQLiteFullException) {
                deleteDataUntil(DatabaseVariables.lastEvaluatedDataPoint)
                _workerThread?.postTask(insert_task)
            }
        } else {
            deleteDataUntil(DatabaseVariables.lastEvaluatedDataPoint)
            _workerThread?.postTask(insert_task)
        }
    }

    fun getAllDataPoints(callback: (List<RawData>) -> Unit) {
        val task = Runnable {
            callback(database().dataPointDAO().getAllDataPoints())
        }
        _workerThread?.postTask(task)
    }

    fun getAmountDataPoints(amount: Int, callback: (List<RawData>) -> Unit) {
        val task = Runnable {
            callback(database().dataPointDAO().getAmountDataPoints(amount))
        }
        _workerThread?.postTask(task)
    }

    fun deleteDataUntil(timestamp: Long) {
        val task = Runnable {
            val pointsDeleted = _database?.dataPointDAO()?.deleteUntilDataPoint(timestamp)
            if (pointsDeleted != null) DatabaseVariables.totalDataPoints -= pointsDeleted
        }
        _workerThread?.postTask(task)
    }

    fun deleteAllDataFromDb() {
        val task = Runnable {
            _database?.dataPointDAO()?.deleteAll()
            DatabaseVariables.totalDataPoints = 0
        }
        _workerThread?.postTask(task)
    }

    fun getAmountEvalDataPoints(amount: Int, timestamp: Long, callback: (List<RawData>) -> Unit) {
        val task = Runnable {
            callback(database().dataPointDAO().getAmountEvalDataPoints(amount, timestamp))
        }
        _workerThread?.postTask(task)
    }

    /**
     * Insert a annotation data point to the database.
     *
     * @param dataPoint an annotation data point, see DataPoints.kt file
     */
    fun insertAnnotationData(dataPoint: AnnotationDataPoint) {
        val insert_task = Runnable {
            _database?.annotationDAO()?.insert(dataPoint)
            DatabaseVariables.totalAnnotationPoints += 1
        }
        _workerThread?.postTask(insert_task)
    }

    /**
     * Insert a event data point to the database.
     *
     * @param dataPoint an event data point, see DataPoints.kt file
     *
    fun insertEventData(dataPoint: EventDataPoint) {
    val insert_task = Runnable {
    _database?.eventDAO()?.insert(dataPoint)
    DatabaseVariables.totalEventPoints += 1
    }
    _workerThread?.postTask(insert_task)
    }
     */

    /**
     * Insert a event probabilities to the database.
     *
     * @param dataPoint an event data point, see DataPoints.kt file
     */
    fun insertEventData(dataPoint: EventProbabilities) {
        val insert_task = Runnable {
            _database?.eventDAO()?.insert(dataPoint)
            DatabaseVariables.totalEventPoints += 1
        }
        _workerThread?.postTask(insert_task)
    }

    /**
     * Get all annotation data points from the internal database
     *
     * @param callback a list of returned annotation data points
     */
    fun getAllAnnPoints(callback: (List<AnnotationDataPoint>) -> Unit) {
        val task = Runnable {
            callback(database().annotationDAO().getAllDataPoints())
        }
        _workerThread?.postTask(task)
    }

    /**
     * Get all event data points from the internal database
     *
     * @param callback a list of returned event data points
     *
    fun getAllEvePoints(callback: (List<EventDataPoint>) -> Unit) {
    val task = Runnable {
    callback(database().eventDAO().getAllDataPoints())
    }
    _workerThread?.postTask(task)
    } */

    /**
     * Get all event probabilities from the internal database
     *
     * @param callback a list of returned event data points
     */
    fun getAllEvePoints(callback: (List<EventProbabilities>) -> Unit) {
        val task = Runnable {
            callback(database().eventDAO().getAllDataPoints())
        }
        _workerThread?.postTask(task)
    }

    fun getAmountEvePoints(amount: Int, callback: (List<EventProbabilities>) -> Unit) {
        val task = Runnable {
            callback(database().eventDAO().getAmountDataPoints(amount))
        }
        _workerThread?.postTask(task)
    }

    fun getAmountAnnPoints(amount: Int, callback: (List<AnnotationDataPoint>) -> Unit) {
        val task = Runnable {
            callback(database().annotationDAO().getAmountDataPoints(amount))
        }
        _workerThread?.postTask(task)
    }

    /**
     * Deletes all datapoints from the internal database.
     */
    fun deleteDataFromDb() {
        val task = Runnable {
            _database?.dataPointDAO()?.deleteAll()
            _database?.annotationDAO()?.deleteAll()
            _database?.eventDAO()?.deleteAll()
            _database?.annoEventDAO()?.deleteAmountPairs(DatabaseVariables.totalPairPoints)
            DatabaseVariables.totalDataPoints = 0
            DatabaseVariables.totalAnnotationPoints = 0
            DatabaseVariables.totalEventPoints = 0
            DatabaseVariables.totalPairPoints = 0
        }
        _workerThread?.postTask(task)
    }

    /**
     * Deletes annotation datapoints until given timestamp
     *
     * @param timestamp the timestamp that defines the last point that will be deleted
     */
    fun deleteAnnotationDataUntil(timestamp: Long) {
        val task = Runnable {
            if (DatabaseVariables.totalAnnotationPoints > 8) {
                val pointsDeleted = _database?.annotationDAO()?.deleteUntilDataPoint(timestamp)
                if (pointsDeleted != null) DatabaseVariables.totalAnnotationPoints -= pointsDeleted
            }
        }
        _workerThread?.postTask(task)
    }

    fun deleteAnnotationDataUntilAmount(timestamp: Long, amount: Int) {
        val task = Runnable {
            if (DatabaseVariables.totalAnnotationPoints > 8) {
                val pointsDeleted = _database?.annotationDAO()?.deleteAmountDataPoints(timestamp, amount)
                if (pointsDeleted != null) DatabaseVariables.totalAnnotationPoints -= pointsDeleted
            }
        }
        _workerThread?.postTask(task)
    }

    /**
     * Marks the database as uninitialized, the worker thread will stop working but the database might still be
     * referenced elsewhere. Therefore use with care
     */
    fun destroy() {
        _workerThread?.join()
        _workerThread = null
        _database = null
    }

    /**
     * Deletes event datapoints until given timestamp
     *
     * @param timestamp the timestamp that defines the last point that will be deleted
     */
    fun deleteEventDataUntil(timestamp: Long) {
        val task = Runnable {
            val pointsDeleted = _database?.eventDAO()?.deleteUntilDataPoint(timestamp)
            if (pointsDeleted != null) DatabaseVariables.totalEventPoints -= pointsDeleted
        }
        _workerThread?.postTask(task)
    }

    fun addEventAnnotationPair(dataPair: AnnotationAndEvent) {
        val task = Runnable {
            _database?.annoEventDAO()?.insert(dataPair)
            DatabaseVariables.totalPairPoints += 1
        }
        _workerThread?.postTask(task)
    }

    fun getEventAnnotationPairs(amount: Int, callback: (List<AnnotationAndEvent>) -> Unit) {
        val task = Runnable {
            callback(database().annoEventDAO().getMatchingPairs(amount))
        }
        _workerThread?.postTask(task)
    }

    fun getPairAmount(callback: (Int) -> Unit) {
        val task = Runnable {
            callback(database().annoEventDAO().getPairAmount())
        }
        _workerThread?.postTask(task)
    }

    fun deleteAmountPairPoints(amount: Int) {
        val task = Runnable {
            val pointsDeleted = _database?.annoEventDAO()?.deleteAmountPairs(amount)
            if (pointsDeleted != null) DatabaseVariables.totalPairPoints -= pointsDeleted
        }
        _workerThread?.postTask(task)
    }
}
