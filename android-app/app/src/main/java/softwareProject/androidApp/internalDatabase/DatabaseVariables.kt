package softwareProject.androidApp.internalDatabase

object DatabaseVariables {

    var totalDataPoints = 0
    var lastEvaluatedDataPoint: Long = 0
    var maxRowsInTable = 4320000
    var totalAnnotationPoints = 0
    var totalEventPoints = 0
    var totalPairPoints = 0
    /**
     * Update the database variables with current database state
     */
    fun updateVariables() {
        SensorDatabase.database().getAllDataPoints { list ->
            totalDataPoints = list.size
        }
        SensorDatabase.database().getAllEvePoints { list ->
            totalEventPoints = list.size
        }
        SensorDatabase.database().getAllAnnPoints { list ->
            totalAnnotationPoints = list.size
        }
        SensorDatabase.database().getPairAmount { num ->
            totalPairPoints = num
        }
    }
}