package softwareProject.androidApp

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Handler
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import softwareProject.androidApp.auth.Authentication
import softwareProject.androidApp.internalDatabase.DatabaseVariables
import softwareProject.androidApp.internalDatabase.DbWorkerThread

open class DataSending(private val sensor: SensorClass) {
    private var isInBackup = false
    private var timeoutCounter = 0
    private var delay: Long = Settings.delay
    private var backupDelay = delay

    private val RawTable = "raw"
    private val eventTable = "event"
    private val annotationTable = "annotation"
    private val queue: RequestQueue by lazy {
        createQueue()
    }
    private val rawHandler = Handler()
    private val eventHandler = Handler()
    private val annHandler = Handler()
    private var mDbWorkerThread: DbWorkerThread = sensor.dbWorkerThread

    init {
        this.scheduleTask(RawTable, delay)
        this.scheduleTask(eventTable, delay)
        this.scheduleTask(annotationTable, delay)
    }

    open fun createQueue(): RequestQueue {
        return Volley.newRequestQueue(sensor)
    }

    /**
     *  Schedule the next time data sending is attempted
     *
     *  @param delay, the delay after which the data sending is attempted
     */
    open fun scheduleTask(db: String, delay: Long) {
        if (db == RawTable) {
            rawHandler.removeCallbacks(null)
            rawHandler.postDelayed({ sendData(db) }, delay)
        } else if (db == eventTable) {
            eventHandler.removeCallbacks(null)
            eventHandler.postDelayed({ sendData(db) }, delay)
        } else if (db == annotationTable) {
            annHandler.removeCallbacks(null)
            annHandler.postDelayed({ sendData(db) }, delay)
        }
    }

    /**
     * The main function that calls for the sendToServer helper function if the phone connected to
     * the internet or creates a listener for checking when the phone comes back online
     *
     * @param db, database that is currently sent
     */
    fun sendData(db: String) {
        delay = Settings.delay
        if (isInBackup) println(backupDelay)
        else println(delay)
        if (!Settings.offline) {
            if (checkConnection()) {
                sendToServer(db)
            } else {
                // The device is not connected, so create a listener to find out when the device is back online
                connectionListener(db)
            }
        } else {
            scheduleTask(db, delay)
        }
    }

    // Checks whether the phone is connected to the internet
    open fun checkConnection(): Boolean {
        val cm = sensor.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return (activeNetwork?.isConnected == true && !Settings.offline)
    }

    /**
     * A helper function that sends the parsed JSON file to the server, and calls for the
     * desired functions based on the server response
     */
    open fun sendToServer(db: String) {
        Authentication.withToken(sensor, { token ->
            mDbWorkerThread.postTask(Runnable {
                val data = ParseJson.genJsonFromDB(db)
                val address = data.third
                val url = "http://${Settings.ipAddress}:80/$address"
                val jsonData = data.first.first
                val collectedAll = data.first.second
                val timestamp = data.second
                jsonData.put("token", token)
                val request = object : StringRequest(Request.Method.POST, url, Response.Listener<String> { response ->
                    successHandling(db, response, timestamp, collectedAll)
                }, Response.ErrorListener { error ->
                    errorHandling(db, error)
                }) {

                    override fun getBodyContentType(): String {
                        return "application/json"
                    }

                    override fun getBody(): ByteArray {
                        return jsonData.toString().toByteArray()
                    }
                }
                queue.add(request)
            })
        }, 0, {
            scheduleTask(db, delay)
        })
    }

    /**
     * Handles the situation when the data is sent successfully
     *
     * @param response the response received from the server
     */
    fun successHandling(db: String, response: String, timestamp: Long, collectedAll: Boolean) {
        Log.i("Success", response)
        // Log.i("ok", "SENT")
        timeoutCounter = 0
        if (isInBackup) {
            isInBackup = false
            backupDelay = delay
        }
        deleteUntilTimestamp(db, timestamp)
        println(db)
        if (collectedAll)
            scheduleTask(db, delay)
        else
            scheduleTask(db, 500)
    }

    private fun deleteUntilTimestamp(db: String, timestamp: Long) {
        when (db) {
            annotationTable ->
                sensor.sensorDb.deleteAnnotationDataUntilAmount(timestamp, DatabaseVariables.totalAnnotationPoints - 8)
            eventTable -> sensor.sensorDb.deleteAnnotationDataUntilAmount(
                timestamp,
                DatabaseVariables.totalAnnotationPoints - 8
            )
            else -> {
                sensor.sensorDb.deleteDataUntil(timestamp)
            }
        }
    }

    /**
     * Handles any errors which can be resolved on the phone's end. These include the situations
     * when the server is unable to parse the sent JSON file, and when the server cannot be reached
     *
     * @param error the type of error that was received
     */
    fun errorHandling(db: String, error: VolleyError) {
        Log.e("Failure", error.toString())
        val errorCode = error.networkResponse
        if (errorCode != null) {
            // Error received from server, i.e. something happened on the phone's end
            val errorDesc = String(error.networkResponse.data)
            Log.e("$db ERROR: ", errorDesc)
            if (errorDesc == "JSON not valid") {
                // The JSON is somehow corrupted, so just delete the current database
                sensor.sensorDb.deleteDataFromDb()
            }
            scheduleTask(db, delay)
        } else {
            // Unable to reach the server
            Log.i("error", "TIMEOUT")
            isInBackup = true
            timeoutCounter += 1
            backupDelay = Math.max((backupDelay / 2), 1000)
            pingServer(db)
        }
    }

    /**
     *  Pings the server until a response is received or offline mode is enabled. Once a response is received, (error or Ok)
     *  attempts to send the data again.
     */
    open fun pingServer(db: String) {
        if (checkConnection()) {
            val r = Runnable {
                val url = "http://${Settings.ipAddress}:80/"
                val stringRequest = StringRequest(Request.Method.GET, url,
                    Response.Listener<String> { response ->
                        Log.d("Response: ", response)
                        scheduleTask(db, 10)
                    },
                    Response.ErrorListener { error ->
                        Log.e("Error: ", error.toString())
                        if (error.networkResponse != null)
                            scheduleTask(db, 10)
                        else {
                            Log.e("Error: ", "UNABLE TO REACH SERVER")
                            pingServer(db)
                        }
                    })
                queue.add(stringRequest)
            }
            rawHandler.removeCallbacks(null)
            rawHandler.postDelayed(r, 5000)
        } else connectionListener(db)
    }

    /**
     * A listener that determines when the device is back online and the data can be sent
     */
    open fun connectionListener(db: String) {
        val c = sensor.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        Log.e("Error: ", "DEVICE OFFLINE")
        c.addDefaultNetworkActiveListener {
            if (c.isDefaultNetworkActive) {
                    Log.d("Status: ", "DEVICE ONLINE")
                    isInBackup = true
                    scheduleTask(db, 500)
            }
        }
    }

    /**
     * Stop sending data
     */
    fun stop() {
        rawHandler.removeCallbacks(null)
        eventHandler.removeCallbacks(null)
        annHandler.removeCallbacks(null)
    }
}