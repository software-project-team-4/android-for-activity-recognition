package softwareProject.androidApp

import android.app.Application
import softwareProject.androidApp.internalDatabase.DatabaseVariables
import softwareProject.androidApp.internalDatabase.SensorDatabase

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        SensorDatabase.init(this)
        DatabaseVariables.updateVariables()
    }
}
