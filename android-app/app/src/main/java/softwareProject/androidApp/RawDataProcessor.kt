package softwareProject.androidApp

import android.hardware.Sensor
import android.hardware.SensorEvent
import softwareProject.androidApp.internalDatabase.AccDataPoint
import softwareProject.androidApp.internalDatabase.GyrDataPoint
import softwareProject.androidApp.internalDatabase.RawData
import softwareProject.androidApp.internalDatabase.SensorDatabase

class RawDataProcessor(database: SensorDatabase, eventActivity: EventActivity) {

    // Acc and gyro Data queue
    private val accQueue = mutableListOf<AccDataPoint>()
    private val gyrQueue = mutableListOf<GyrDataPoint>()
    private val database = database
    private val eventActivity = eventActivity

    /**
     * Adds acc and gyr sensor values to queue, if neither list reaches 250 members runs the classification
     * params:
     * @event: The sensor event recorded by the divice
     * @eventTime: The time when the event happened
     */
    fun addDataToQueue(event: SensorEvent, eventTime: Long) {
        val sensor = event.sensor
        val x = event.values[0]
        val y = event.values[1]
        val z = event.values[2]
        if (sensor.type == Sensor.TYPE_ACCELEROMETER) {
            val dataPoint = AccDataPoint(eventTime, x, y, z)
            accQueue += dataPoint
            // Triggers Detector (Detects only one datapoint) //TODO: What is this??
            // detector.processFalls(mutableListOf(dataPoint), sensorDb, dbWorkerThread)
        } else if (sensor.type == Sensor.TYPE_GYROSCOPE) {
            val dataPoint = GyrDataPoint(eventTime, x, y, z)
            gyrQueue += dataPoint
        }
        if (accQueue.size >= 250 || gyrQueue.size >= 250) {
            classifyAndInsertToDatabase(accQueue, gyrQueue)
            accQueue.clear()
            gyrQueue.clear()
        }
    }

    /**
     * Adds the matched raw data to the database and calls the EventActivity for event classification
     * params:
     * @accQueue: The queue of accelerator data points
     * @gyrQueue: The queue of gyroscope data points
     */
    private fun classifyAndInsertToDatabase(accQueue: List<AccDataPoint>, gyrQueue: List<GyrDataPoint>) {
        val matchedRawData: List<RawData> = matchValues(accQueue, gyrQueue)
        for (rawData in matchedRawData) {
            database.insertDataPoint(rawData)
        }
        eventActivity.runModel(matchedRawData)
    }

    /**
     * Create a list of RawData points from the matched float arrays of gyr and acc data
     * params:
     * @timestamps: The timestamps from the longer of the queues
     * @accQueue: The queue of accelerator sensor values
     * @gyrQueue: The queue of gyroscope sensor values
     */
    private fun createListOfPoints(timestamps: List<Long>, accQueue: List<FloatArray>, gyrQueue: List<FloatArray>): List<RawData> {
        val size = timestamps.size
        val dataPointList = (0..size - 1).map { i ->
            RawData(timestamps[i], accQueue[i][0], accQueue[i][1], accQueue[i][2], gyrQueue[i][0], gyrQueue[i][1], gyrQueue[i][2])
        }
        return dataPointList
    }

    /**
     * The higher level function for mathching acc and gyr points
     * params:
     * @accQueue: The queue of accelerator sensor values, not matched
     * @gyrQueue: The queue of gyroscope sensor values, not matched
     */
    fun matchValues(accQueue: List<AccDataPoint>, gyrQueue: List<GyrDataPoint>): List<RawData> {
        val accQueueSize = accQueue.size
        val gyrQueueSize = gyrQueue.size
        val accSensorValues: List<FloatArray> = accQueue.map { v -> floatArrayOf(v.acc_x, v.acc_y, v.acc_z) }
        val gyrSensorValues: List<FloatArray> = gyrQueue.map { v -> floatArrayOf(v.gyr_x, v.gyr_y, v.gyr_z) }
        lateinit var timestamps: List<Long>
        lateinit var matchedDataPoints: List<RawData>
        if (accQueueSize > gyrQueueSize) {
            val lSpace = linspace(accQueueSize, gyrQueueSize)
            timestamps = accQueue.map { gyrDataPoint -> gyrDataPoint.timestamp }
            val interpolatedGyrQueue = queueInterpolation(gyrSensorValues, lSpace, accQueueSize)
            matchedDataPoints = createListOfPoints(timestamps, accSensorValues, interpolatedGyrQueue)
        } else {
            val lSpace = linspace(gyrQueueSize, accQueueSize)
            timestamps = gyrQueue.map { gyrDataPoint -> gyrDataPoint.timestamp }
            val interpolatedAccQueue = queueInterpolation(accSensorValues, lSpace, gyrQueueSize)
            matchedDataPoints = createListOfPoints(timestamps, interpolatedAccQueue, gyrSensorValues)
        }
        return matchedDataPoints
    }

    /**
     * Fills the smaller queue with the needed amount of average points
     * params:
     * @queue: The queue of sensor values from the smaller queue (acc or gyr)
     * @linspace: The new indexes for the values in the smaller queue
     * @largerSize: The size of the larger of the list (should always be 250!!)
     */
    private fun queueInterpolation(queue: List<FloatArray>, linspace: List<Int>, largerSize: Int): List<FloatArray> {
        val interpolatedQueue = mutableListOf<FloatArray>()
        var previousValue: FloatArray
        var nextValue = queue[0]
        val valuesToBeFilled = mutableListOf<Int>()
        for (v: Int in (1..largerSize)) {
            val valueIndex: Int = linspace.indexOf(v)
            if (valueIndex == -1) {
                valuesToBeFilled += v
            } else {
                previousValue = nextValue
                nextValue = queue[valueIndex]
                val averageValue: FloatArray = averageDataPoint(previousValue, nextValue)
                for (v2 in valuesToBeFilled) {
                    interpolatedQueue += averageValue
                }
                valuesToBeFilled.clear()
                interpolatedQueue += nextValue
            }
        }
        return interpolatedQueue.toList()
    }

    /**
     * Counts the average value between two sensor data points
     * params:
     * @point1: The sensor value for the first point
     * @point2: The sensor value for the seccond point
     */
    private fun averageDataPoint(point1: FloatArray, point2: FloatArray): FloatArray {
        val averagePoint = mutableListOf<Float>()
        for (v in 0..2) {
            val averageValue: Float = (point1[v] + point2[v]) / 2.toFloat()
            averagePoint += averageValue
        }
        return averagePoint.toFloatArray()
    }

    /**
     * Counts the new indexes for the values in the smaller queue
     * params:
     * @largerSize: The size of the larger queue (Should be always 250!!)
     * @smallerSize: The size of the smaller queue
     */
    private fun linspace(largerSize: Int, smallerSize: Int): List<Int> {
        val one_bucket = (largerSize - 1) / (smallerSize - 1).toDouble()
        val mapping_values = (1..smallerSize).map { v ->
            if (v == 1) {
                v
            } else if (v == smallerSize) {
                largerSize
            } else {
                1 + Math.floor((v - 1) * one_bucket).toInt()
            }
        }
        return mapping_values
    }
}