package softwareProject.androidApp

import android.util.Log
import com.google.firebase.FirebaseApp
import com.google.firebase.ml.common.modeldownload.FirebaseLocalModelSource
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager
import com.google.firebase.ml.custom.FirebaseModelInterpreter
import com.google.firebase.ml.custom.FirebaseModelOptions
import com.google.firebase.ml.custom.FirebaseModelInputOutputOptions
import com.google.firebase.ml.custom.FirebaseModelDataType
import com.google.firebase.ml.custom.FirebaseModelInputs
import softwareProject.androidApp.internalDatabase.AnnotationAndEvent
import softwareProject.androidApp.internalDatabase.DatabaseVariables
import softwareProject.androidApp.internalDatabase.EventProbabilities
import softwareProject.androidApp.internalDatabase.RawData
import java.util.Arrays

open class EventActivity(val sensor: SensorClass) {
    var returnPoint = "" // A debug variable that is used mainly in unit test
    lateinit var dataOptions: FirebaseModelInputOutputOptions
    lateinit var interpreter: FirebaseModelInterpreter
    var timestamp_start = 0L
    val database = sensor.sensorDb
    var latest: Array<FloatArray> = emptyArray()

    init {
        initialize()
    }

    open fun initialize() {
        FirebaseApp.initializeApp(sensor)
        // Input is a 1x1 Tensor
        val inputDims = intArrayOf(250, 6)
        // Output is a 1x1 Tensor
        val outputDims = intArrayOf(1, Settings.outputSize)

        // Define the Input and Output dimensions and types
        dataOptions = FirebaseModelInputOutputOptions.Builder()
            .setInputFormat(0, FirebaseModelDataType.FLOAT32, inputDims)
            .setOutputFormat(0, FirebaseModelDataType.FLOAT32, outputDims)
            .build()

        // Load mode from local assets storage
        val localModelSource = FirebaseLocalModelSource.Builder("asset")
            .setAssetFilePath(Settings.path2).build()

        val manager = FirebaseModelManager.getInstance()
        manager.registerLocalModelSource(localModelSource)

        val options = FirebaseModelOptions.Builder()
            .setLocalModelName("asset")
            .build()

        interpreter = FirebaseModelInterpreter.getInstance(options) ?: error("Null Interpreter")

        // Pass the input data, as an array, that contains a single float array, with a single value
        // So in all [0][0] = 21, a 1x1 matrix in the form a Array of FloatArray
        //  val inp = arrayOf(floatArrayOf(21f))
        // val value = floatArrayOf(0f, 0f, 0f, 0f, 0f, 0f)
        // val inp: Array<FloatArray> = (1..250).map {num -> value}.toTypedArray()
    }

    fun convertToFloat(data: List<RawData>): Array<FloatArray> {
        val res = Array(250) { FloatArray(6) }
        for (i: Int in 0 until 250) {
            // last = Math.max(last, acc[i].timestamp)
            res[i][0] = data[i].acc_x
            res[i][1] = data[i].acc_y
            res[i][2] = data[i].acc_z
            res[i][3] = data[i].gyro_x
            res[i][4] = data[i].gyro_y
            res[i][5] = data[i].gyro_z
        }
        timestamp_start = data.first().timestamp
        DatabaseVariables.lastEvaluatedDataPoint = data.last().timestamp
        return res
    }

    open fun runModel(data: List<RawData>) {
        val input = convertToFloat(data)

        Log.d("EventActivity", "input array: ${Arrays.deepToString(input)}")

        val inputs = FirebaseModelInputs.Builder().add(input).build()
        interpreter.run(inputs, dataOptions)
            .continueWith { task ->
                try {
                    // The output is also a Array of FloatArray
                    val output = task.result!!.getOutput<Array<FloatArray>>(0)
                    latest = output
                    val event_name = sensor.changeProbs(output[0])
                    val pair = AnnotationAndEvent(System.currentTimeMillis(), sensor.currentAnnotation(), event_name)
                    database.addEventAnnotationPair(pair)
                    if (DatabaseVariables.totalPairPoints > 8) {
                        database.deleteAmountPairPoints(DatabaseVariables.totalPairPoints - 8)
                    }
                    // Result is in [0][0]
                    println(Arrays.deepToString(output))
                    val event = EventProbabilities(
                        timestamp_start,
                        DatabaseVariables.lastEvaluatedDataPoint,
                        "sensorflow_3.1",
                        Arrays.deepToString(output)
                    )
                    database.insertEventData(event)
                    Log.d("EventActivity", "Classification result: ${Arrays.deepToString(output)}")
                } catch (t: Throwable) {
                    Log.d("EventActivity", t.localizedMessage, t)
                }
            }
    }
}