package softwareProject.androidApp

/**
 * An object to store variables (delay, intervalIndex) for both activities
 */

object Settings {
    var delay: Long = 10000
    var intervalIndex = 2
    var startText = "Current Interval :10 s"
    var ipAddress = "3.86.188.116"
    var currentAnnotation = R.id.stopButton
    var oldAnnotation = R.id.stopButton
    var path = "converted_model.tflite"
    var path2 = "sensorflow_3.1.tflite"
    var outputSize = 3
    var annotationStartTime: Long = 0
    var offline = false
    var stop = false
    var nnModels = arrayOf("SensorFlow_Lit_1.3", "SensorFlow_Lit_2.6")
    var currentModel = nnModels[1]
    var modelHasChanged = false
}