package softwareProject.androidApp

import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.View.SYSTEM_UI_FLAG_FULLSCREEN
import android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.main_screen.*
import softwareProject.androidApp.detector.Detector
import softwareProject.androidApp.internalDatabase.AnnotationDataPoint
import softwareProject.androidApp.internalDatabase.DatabaseVariables
import softwareProject.androidApp.internalDatabase.DbWorkerThread
import softwareProject.androidApp.internalDatabase.SensorDatabase
import kotlin.math.roundToInt

open class SensorClass : AppCompatActivity(), SensorEventListener {

    // Sensor variables
    private lateinit var mSensorManager: SensorManager
    private var mAcc: Sensor? = null
    private var mGyro: Sensor? = null
    // Raw data processor
    private lateinit var rawDataProcessor: RawDataProcessor
    // Database variables
    var sensorDb: SensorDatabase = SensorDatabase.database()
    var dbWorkerThread: DbWorkerThread = SensorDatabase.workerThread()
    private var detector: Detector = Detector()
    private val mUiHandler = Handler()
    private val handler = Handler()
    private lateinit var dataSending: DataSending
    val database: SensorDatabase = SensorDatabase.database()
    private lateinit var eventActivity: EventActivity
    var nn_state = 0

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_screen)
        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mGyro = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
        mAcc = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        mSensorManager.registerListener(this, mGyro, SensorManager.SENSOR_DELAY_GAME)
        mSensorManager.registerListener(this, mAcc, SensorManager.SENSOR_DELAY_GAME)
        ParseJson.sensorDb = sensorDb
        eventActivity = EventActivity(this)
        rawDataProcessor = RawDataProcessor(database, eventActivity)
        dataSending = DataSending(this)
        fillCurrentValues()
    }

    private lateinit var idToAnnotate: Map<Int, CharSequence>
    private lateinit var probabilityTexts: Array<TextView>

    // RunProb = Standing , RunProb2 = Sitting, bikProb, walProb, starisDownProb
    fun fillCurrentValues() {
        idToAnnotate = mapOf(
            Annotation1.id to resources.getText(R.string.standing_annotation),
            Annotation2.id to resources.getText(R.string.sitting_annotation),
            Annotation3.id to resources.getText(R.string.walking_annotation),
            Annotation6.id to resources.getText(R.string.walkingDown_annotation),
            Annotation5.id to resources.getText(R.string.walkingUp_annotation),
            Annotation4.id to resources.getText(R.string.biking_annotation),
            stopButton.id to resources.getText(R.string.no_annotation)
        )
        if (Settings.currentModel == Settings.nnModels[0]) {
            Annotation4.visibility = View.VISIBLE
            Annotation6.visibility = View.VISIBLE
            Annotation5.visibility = View.VISIBLE
            EventProb4.visibility = View.VISIBLE
            EventProb5.visibility = View.VISIBLE
            EventProb6.visibility = View.VISIBLE
            probabilityTexts = arrayOf(EventProb1, EventProb2, EventProb3, EventProb5, EventProb6, EventProb4)
        } else {
            Annotation4.visibility = View.GONE
            Annotation6.visibility = View.GONE
            Annotation5.visibility = View.GONE
            EventProb4.visibility = View.GONE
            EventProb5.visibility = View.GONE
            EventProb6.visibility = View.GONE
            probabilityTexts = arrayOf(EventProb1, EventProb2, EventProb3)
        }
    }

    /**
     * Function for the "Log" button to change the view
     */

    fun changeToLogActivity(@Suppress("UNUSED_PARAMETER") view: View) {
        val intent = Intent(this, LogActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_left2, R.anim.slide_left)
    }

    /**
     * Function for the Annotation buttons to change the current Annotation
     * Calls:
     * -highlightAnnotation function to change UI to correspond the Current annotation
     * -storeAnnotation to store the the annotation to internal database
     */

    fun changeEvent(view: View) {
        if (idToAnnotate.containsKey(view.id)) {
            if (Settings.currentAnnotation != stopButton.id && view.id != Settings.currentAnnotation) {
                val currentTime = System.currentTimeMillis()
                val dataPoint = AnnotationDataPoint(
                    Settings.annotationStartTime,
                    currentTime,
                    idToAnnotate[Settings.currentAnnotation].toString()
                )
                database.insertAnnotationData(dataPoint)
            }
            if (view.id != Settings.currentAnnotation) {
                Settings.oldAnnotation = Settings.currentAnnotation
                Settings.currentAnnotation = view.id
                Settings.annotationStartTime = System.currentTimeMillis()
                if (view.id != stopButton.id) {
                    view.background = resources.getDrawable(R.drawable.textbg_tint)
                }
                if (Settings.oldAnnotation != stopButton.id) {
                    val oldView = findViewById<Button>(Settings.oldAnnotation)
                    oldView.background = resources.getDrawable(R.drawable.textbg)
                }
            }
        }
    }

    /**
     * Change the prediction values to the ui
     * params:
     * @probArray: Array consisting of double values from the neural network output
     * @returns: The name of the Event that had the largest accuracy
     */

    fun changeProbs(probArray: FloatArray): String {
        var i = 0
        val sum = probArray.sum()
        var highest = -1
        var highestIndex = -1
        println(probArray)
        while (i < probabilityTexts.size) {
            val value = ((probArray[i] / sum) * 100).roundToInt()
            if (value > highest) {
                highest = value
                highestIndex = i
            }
            probabilityTexts[i].text = ("$value %")
            i += 1
        }
        if (Settings.currentModel == Settings.nnModels[0]) {
            var StringArray = arrayOf("Standing", "Sitting", "Walking", "Stairs Up", "Stairs Down", "Biking")
            return StringArray[highestIndex]
        } else {
            var StringArray2 = arrayOf("Standing", "Sitting", "Walking")
            return StringArray2[highestIndex]
        }
    }

    fun currentAnnotation(): String {
        return idToAnnotate[Settings.currentAnnotation].toString()
    }

    /**
     * Exit the activity
     */

    fun exit() {
        mSensorManager.unregisterListener(this)
        dataSending.stop()
        handler.removeCallbacks(null)
        finish()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        exit()
    }

    /**
     * A necessary function for a SensorEventListener
     */
    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
    }

    /**
     * Save new sensor values to phone's internal database by using a side thread.
     * The function is ran when ever the values of one followed sensor change.
     *
     * @param event information about the sensor, which value was changed
     */
    override fun onSensorChanged(event: SensorEvent) {
        // Set the string values of senso x, senso y, senso z to the corresponding
        // values.
        val eventTime = System.currentTimeMillis()
        val task = Runnable { rawDataProcessor.addDataToQueue(event, eventTime) }
        dbWorkerThread.postTask(task)
    }

    /**
     * Creates a runnable for fetching all the datapoints from the database and posts the task to a DbWorkerThread.
     *
     * @param view a necessary parameter for functions used as onClick function
     */
    fun fetchDataFromDb(@Suppress("UNUSED_PARAMETER") view: View) {
        val task = Runnable {
            val dataLength = DatabaseVariables.totalDataPoints
            mUiHandler.post {
                if (dataLength == 0) {
                    Toast.makeText(this, "No data in cache..!!", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(
                        this,
                        "There are $dataLength datapoints in the database",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
        dbWorkerThread.postTask(task)
    }

    override fun onResume() {
        super.onResume()
        if (Settings.modelHasChanged) {
            Settings.modelHasChanged = false
            if (Settings.currentModel == Settings.nnModels[0]) {
                eventActivity = EventActivity(this)
                rawDataProcessor = RawDataProcessor(database, eventActivity)
            } else if (Settings.currentModel == Settings.nnModels[1]) {
                eventActivity = EventActivity(this)
                rawDataProcessor = RawDataProcessor(database, eventActivity)
            }
            fillCurrentValues()
        }
        window.decorView.apply {
            systemUiVisibility =
                View.SYSTEM_UI_FLAG_IMMERSIVE or SYSTEM_UI_FLAG_FULLSCREEN or SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }
    }

    /**
     * Function to start the settings activity
     */
    fun toSettingsView(@Suppress("UNUSED_PARAMETER") view: View) {
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_right2, R.anim.slide_right)
    }

    fun toMonitoringView(@Suppress("UNUSED_PARAMETER") view: View) {
        val intent = Intent(this, MonitoringActivity::class.java)
        startActivity(intent)
    }
}
