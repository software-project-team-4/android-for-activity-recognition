package softwareProject.androidApp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.SeekBar
import android.widget.Switch
import android.widget.Toast
import kotlinx.android.synthetic.main.sensor_config.*
import softwareProject.androidApp.internalDatabase.SensorDatabase

open class SettingsActivity : AppCompatActivity() {

    var sensorDb: SensorDatabase = SensorDatabase.database()
    val delayList = arrayOf(2000, 5000, 10000, 30000, 60000)
    lateinit var switch: Switch

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sensor_config)
        switch = findViewById(R.id.switch1)
        switch.isChecked = Settings.offline
        switch.setOnCheckedChangeListener { _, isChecked ->
            run {
                Settings.offline = isChecked
                Log.i("", "OFFLINE MODE $isChecked")
            }
        }
        intervalText.text = Settings.startText
        seekBar.progress = Settings.intervalIndex
        iptext.setText(Settings.ipAddress)
        if (Settings.currentModel == Settings.nnModels[1]) {
            nn_switch.isChecked = true
        }
        nn_switch.setOnCheckedChangeListener { _, isChecked ->
            run {
                Settings.modelHasChanged = true
                if (isChecked) {
                    Settings.currentModel = Settings.nnModels[1]
                } else {
                    Settings.currentModel = Settings.nnModels[0]
                }
            }
        }
        window.decorView.apply {
            systemUiVisibility =
                View.SYSTEM_UI_FLAG_IMMERSIVE or View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }
        /**
         * SeekBar listener for changing the value of the delay interval
         */
        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            /**
             * @params i = the new index of the seekBar
             */

            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                val index = kotlin.math.min(i, delayList.size - 1)
                Settings.intervalIndex = index
                Settings.delay = delayList[index].toLong()
                val sec = Settings.delay / 1000
                intervalText.text = "Current Interval :$sec s"
                Settings.startText = "Current Interval :$sec s"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
        seekBar.progress = Settings.intervalIndex
    }

    /**
     * Function to go back to monitoring sensors
     * @params Mandatory parameter for changing views from UI
     */
    fun toSendingView(@Suppress("unused_parameter") view: View) {
        Settings.ipAddress = iptext.text.toString()
        Toast.makeText(this, "Settings saved succesfully", Toast.LENGTH_LONG).show()
        finish()
        overridePendingTransition(R.anim.slide_left2, R.anim.slide_left)
    }

    fun toLogActivity(@Suppress("UNUSED_PARAMETER") view: View) {
        val intent = Intent(this, LogActivity::class.java)
        finish()
        overridePendingTransition(R.anim.slide_left2, R.anim.slide_left)
        startActivity(intent)
    }

    fun deleteData(@Suppress("UNUSED_PARAMETER") view: View) {
        sensorDb.deleteDataFromDb()
    }
}