package softwareProject.androidApp

import android.os.Bundle
import com.android.volley.Network
import com.android.volley.NetworkResponse
import com.android.volley.RequestQueue
import com.android.volley.ResponseDelivery
import com.android.volley.TimeoutError
import com.android.volley.VolleyError
import com.android.volley.toolbox.NoCache
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows
import org.robolectric.annotation.Implementation
import org.robolectric.annotation.Implements
import softwareProject.androidApp.internalDatabase.DbWorkerThread
import softwareProject.androidApp.internalDatabase.RawData
import softwareProject.androidApp.internalDatabase.EventProbabilities
import softwareProject.androidApp.internalDatabase.DatabaseVariables
import softwareProject.androidApp.internalDatabase.SensorDatabase
import softwareProject.androidApp.internalDatabase.AnnotationDataPoint
import softwareProject.androidApp.internalDatabaseTests.SensorDatabaseTest

@RunWith(RobolectricTestRunner::class)
class DataSendingTests : SensorDatabaseTest() {

    private lateinit var activity: MockSend
    private lateinit var worker: DbWorkerThread
    private lateinit var sensorclass: SensorClass
    val rawTable = "raw"
    val eventDb = "event"
    val annotationDb = "annotation"

    @Implements(DataSending::class)
    class MockSend(sensorClass: SensorClass) : DataSending(sensorClass) {
        var delay1 = 0L
        var hasSent = false
        var isListening = false
        var online = false
        var isPinging = false
        private val mDelivery: ResponseDelivery? = null
        private val mMockNetwork: Network? = null

        @Implementation
        override fun createQueue(): RequestQueue {
            return RequestQueue(NoCache(), mMockNetwork, 0, mDelivery)
        }

        @Implementation
        override fun sendToServer(db: String) {
            hasSent = true
        }

        @Implementation
        override fun pingServer(db: String) {
            isPinging = true
        }

        @Implementation
        override fun checkConnection(): Boolean {
            return online
        }

        @Implementation
        override fun connectionListener(db: String) {
            isListening = true
        }

        @Implementation
        override fun scheduleTask(db: String, delay: Long) {
            delay1 = delay
        }
    }

    class MockSensorClass() : SensorClass() {

        override fun onCreate(savedInstanceState: Bundle?) {
            // super.onCreate(savedInstanceState)
            // setContentView(R.layout.sensor_started)
            ParseJson.sensorDb = sensorDb
        }
    }

    @Before
    fun setUp() {
        worker = SensorDatabase.workerThread()
        looper = Shadows.shadowOf(worker.looper)
        sensorclass = Robolectric.buildActivity(MockSensorClass::class.java).create().get()
        activity = MockSend(sensorclass)
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
    }

    @Test
    fun `should delete on success`() {
        db.insertDataPoint(RawData(1398902555536968492L, 123.25F, 54.34F, 23.41F, 123.25F, 54.34F, 23.41F))
        looper.runToEndOfTasks()
        activity.successHandling(rawTable, "Ok", ParseJson.genRawJson().second, true)
        looper.runToEndOfTasks()
        val dataPointCount = DatabaseVariables.totalDataPoints
        println("DatapointDb: $dataPointCount")
        Assert.assertEquals(0, dataPointCount)
        Assert.assertEquals(10000, activity.delay1)
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
    }

    fun `should delete correct database on success`() {
        db.insertDataPoint(RawData(1398902555536968492L, 123.25F, 54.34F, 23.41F, 123.25F, 54.34F, 23.41F))
        looper.runToEndOfTasks()
        db.insertEventData(EventProbabilities(1398902555536968492L, 1398902555536968495L, "walking_model", ""))
        looper.runToEndOfTasks()
        db.insertAnnotationData(AnnotationDataPoint(1398902555536968492L, 1398902555536968495L, "walking"))
        looper.runToEndOfTasks()
        activity.successHandling(rawTable, "Ok", ParseJson.genRawJson().second, true)
        looper.runToEndOfTasks()
        val accPointCount = DatabaseVariables.totalDataPoints
        val eventPointCount = DatabaseVariables.totalEventPoints
        val annotationPointCount = DatabaseVariables.totalAnnotationPoints
        println("Acc: $accPointCount \t Event: $eventPointCount \t Annotation: $annotationPointCount")
        Assert.assertEquals(0, accPointCount)
        Assert.assertEquals(1, eventPointCount)
        Assert.assertEquals(1, annotationPointCount)
        Assert.assertEquals(10000, activity.delay1)
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
    }

    @Test
    fun `should delete until timestamp`() {
        db.insertDataPoint(RawData(1398902555536968492L, 123.25F, 54.34F, 23.41F, 123.25F, 54.34F, 23.41F))
        looper.runToEndOfTasks()
        activity.successHandling(rawTable, "Ok", ParseJson.genRawJson().second, true)
        db.insertDataPoint(RawData(1398902555536968493L, 123.25F, 54.34F, 23.41F, 123.25F, 54.34F, 23.41F))
        looper.runToEndOfTasks()
        val accPointCount = DatabaseVariables.totalDataPoints
        Assert.assertEquals(1, accPointCount)
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
    }

    @Test
    fun `should start pinging on timeout`() {
        activity.isPinging = false
        activity.errorHandling(rawTable, TimeoutError())
        Assert.assertEquals(true, activity.isPinging)
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
    }

    @Test
    fun `should start listening when offline`() {
        activity.online = false
        activity.sendData("raw")
        Assert.assertEquals(true, activity.isListening)
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
    }

    @Test
    fun `should delete all when json invalid`() {
        val accPoint = RawData(1398902555536968492L, 123.25F, 54.34F, 23.41F, 123.25F, 54.34F, 23.41F)
        db.insertDataPoint(accPoint)
        looper.runToEndOfTasks()
        val response = NetworkResponse(400, "JSON not valid".toByteArray(), null, true)
        activity.errorHandling(rawTable, VolleyError(response))
        looper.runToEndOfTasks()
        val pointCount = DatabaseVariables.totalDataPoints
        Assert.assertEquals(0, pointCount)
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
    }

    @Test
    fun `should send data when online`() {
        activity.hasSent = false
        activity.online = true
        activity.sendData("raw")
        Assert.assertEquals(activity.hasSent, true)
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
    }
}
