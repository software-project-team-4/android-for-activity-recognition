package softwareProject.androidApp

import android.os.Bundle
import android.widget.Button
import androidx.test.core.app.ApplicationProvider
import kotlinx.android.synthetic.main.main_screen.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows
import org.robolectric.shadows.ShadowLooper
import softwareProject.androidApp.internalDatabase.DbWorkerThread
import softwareProject.androidApp.internalDatabase.SensorDatabase

@RunWith(RobolectricTestRunner::class)
class AnnotationActivityTest {
    private lateinit var activity: SensorClass
    private lateinit var worker: DbWorkerThread
    private lateinit var db: SensorDatabase
    private lateinit var looper: ShadowLooper

    private lateinit var otherEvent: Button
    private lateinit var standingEvent: Button
    private lateinit var noEvent: Button
    private lateinit var resetButton: Button

    class MockSensor() : SensorClass() {
        override fun onCreate(savedInstanceState: Bundle?) {
            setContentView(R.layout.main_screen)
        }
    }

    @Before
    fun setUp() {
        // Use in memory database
        SensorDatabase.init(ApplicationProvider.getApplicationContext(), true)
        worker = SensorDatabase.workerThread()

        activity = Robolectric.buildActivity(MockSensor::class.java).create().get()

        looper = Shadows.shadowOf(worker.looper)

        standingEvent = activity.Annotation1
        otherEvent = activity.Annotation4
        noEvent = activity.stopButton
        resetButton = activity.stopButton
        db = activity.database
    }

    private fun reset() {
        db.annotationDAO().deleteAll()
        Settings.currentAnnotation = noEvent.id
        Settings.oldAnnotation = noEvent.id
        Settings.annotationStartTime = 0
    }

    @Test
    fun `Inserts running event when running is selected`() {
        try {
            activity.fillCurrentValues()
            activity.changeEvent(view = standingEvent)
            activity.changeEvent(view = resetButton)
            looper.runToEndOfTasks()
            val points = db.annotationDAO().getAllDataPoints()
            assert(points.size == 1 && points.last().event_name == "Standing")
        } finally {
            reset()
        }
    }
}