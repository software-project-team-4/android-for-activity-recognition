package softwareProject.androidApp

import org.json.JSONObject
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import softwareProject.androidApp.internalDatabase.AnnotationDataPoint
import softwareProject.androidApp.internalDatabase.EventProbabilities
import softwareProject.androidApp.internalDatabase.RawData
import softwareProject.androidApp.internalDatabaseTests.SensorDatabaseTest

@RunWith(RobolectricTestRunner::class)
class ParseJsonTest : SensorDatabaseTest() {

    private lateinit var parse: ParseJson

    @Before
    fun setUp() {
        parse = ParseJson
        parse.sensorDb = db
    }

    val timestamps = longArrayOf(1398902555536968492L, 1398902555536968586L, 1398902555536968600L)
    val vectors = floatArrayOf(123.25F, 54.34F, 23.41F)

    @Test
    fun `should parse data point correctly`() {
        val datapoint = RawData(
            timestamps[0], 123.25F, 54.34F, 23.41F,
            123.25F, 54.34F, 23.41F)
        db.insertDataPoint(datapoint)
        looper.runToEndOfTasks()
        val res = parse.fetchDataToJson()
        println(res)
        val data = res.first.getJSONObject(0)
        val ts = data.getLong("timestamp")
        val acc_x = data.getDouble("acc_x").toFloat()
        val acc_y = data.getDouble("acc_y").toFloat()
        val acc_z = data.getDouble("acc_z").toFloat()
        val gyr_x = data.getDouble("gyr_x").toFloat()
        val gyr_y = data.getDouble("gyr_y").toFloat()
        val gyr_z = data.getDouble("gyr_z").toFloat()
        Assert.assertEquals(timestamps[0], ts)
        Assert.assertEquals(vectors[0], acc_x)
        Assert.assertEquals(vectors[1], acc_y)
        Assert.assertEquals(vectors[2], acc_z)
        Assert.assertEquals(vectors[0], gyr_x)
        Assert.assertEquals(vectors[1], gyr_y)
        Assert.assertEquals(vectors[2], gyr_z)
    }

    @Test
    fun `should parse event data correctly`() {
        db.insertEventData(
            EventProbabilities(
                timestamps[0],
                timestamps[1],
                "asd",
                "[0.1545, 2.54E-5, 0.034, 0.0084, 0.5546, 26.37E-17]"
            )
        )
        looper.runToEndOfTasks()
        val res = parse.fetchEventDataToJson()
        val data = res.first.getJSONObject(0)
        val start = data.get("timestamp_start")
        val end = data.get("timestamp_end")
        val activity = data.get("model_name")
        val probs = data.get("probabilities")
        val collectedAll = res.third
        Assert.assertEquals(true, collectedAll)
        Assert.assertEquals(timestamps[0], start)
        Assert.assertEquals(timestamps[1], end)
        Assert.assertEquals("asd", activity)
        Assert.assertEquals("[0.1545, 2.54E-5, 0.034, 0.0084, 0.5546, 26.37E-17]", probs)
    }

    @Test
    fun `should parse annotation data correctly`() {
        db.insertAnnotationData(AnnotationDataPoint(timestamps[0], timestamps[1], "walking"))
        looper.runToEndOfTasks()
        val res = parse.fetchAnnotationDataToJson()
        val data = res.first.getJSONObject(0)
        val start = data.get("timestamp_start")
        val end = data.get("timestamp_end")
        val activity = data.get("event_name")
        val collectedAll = res.third
        Assert.assertEquals(true, collectedAll)
        Assert.assertEquals(timestamps[0], start)
        Assert.assertEquals(timestamps[1], end)
        Assert.assertEquals("walking", activity)
    }

    @Test
    fun `should generate correct raw data triple`() {
        db.insertDataPoint(RawData(timestamps[0], 123.25F, 54.34F, 23.41F, 123.25F, 54.34F, 23.41F))
        looper.runToEndOfTasks()
        val res = parse.genRawJson()
        val correct = JSONObject()
        val pair = parse.fetchDataToJson()
        val acc = pair.first
        val time = pair.second
        correct.put("data", acc)
        Assert.assertEquals(correct.toString(), res.first.first.toString())
        Assert.assertEquals(time, res.second)
        Assert.assertEquals("insertData", res.third)
    }

    @Test
    fun `should generate correct annotation data triple`() {
        db.insertAnnotationData(AnnotationDataPoint(timestamps[0], timestamps[1], "walking"))
        db.insertAnnotationData(AnnotationDataPoint(timestamps[1], timestamps[2], "fall"))
        looper.runToEndOfTasks()
        val res = parse.genAnnotationJson()
        val correct = JSONObject()
        val pair = parse.fetchAnnotationDataToJson()
        val data = pair.first
        val time = pair.second
        correct.put("data", data)
        Assert.assertEquals(correct.toString(), res.first.first.toString())
        Assert.assertEquals(time, res.second)
        Assert.assertEquals("annotationData", res.third)
    }

    @Test
    fun `should generate correct event data triple`() {
        db.insertEventData(EventProbabilities(timestamps[0], timestamps[1], "walking", ""))
        db.insertEventData(EventProbabilities(timestamps[1], timestamps[2], "fall", ""))
        val res = parse.genEventJson()
        val correct = JSONObject()
        val pair = parse.fetchEventDataToJson()
        val data = pair.first
        val time = pair.second
        correct.put("data", data)
        Assert.assertEquals(correct.toString(), res.first.first.toString())
        Assert.assertEquals(time, res.second)
        Assert.assertEquals("eventData", res.third)
    }

    @Test
    fun `should return the correct triple in genJsonFromDb`() {
        db.insertDataPoint(RawData(timestamps[0], 123.25F, 54.34F, 23.41F, 123.25F, 54.34F, 23.41F))
        db.insertAnnotationData(AnnotationDataPoint(timestamps[0], timestamps[1], "walking"))
        db.insertEventData(EventProbabilities(timestamps[0], timestamps[1], "walking", ""))

        val expectedRawData = parse.genRawJson()
        val rawData = parse.genJsonFromDB("raw")
        Assert.assertEquals(expectedRawData.first.toString(), rawData.first.toString())
        Assert.assertEquals(expectedRawData.second, rawData.second)
        Assert.assertEquals(expectedRawData.third, rawData.third)

        val expectedAnnData = parse.genAnnotationJson()
        val annData = parse.genJsonFromDB("annotation")
        Assert.assertEquals(expectedAnnData.first.toString(), annData.first.toString())
        Assert.assertEquals(expectedAnnData.second, annData.second)
        Assert.assertEquals(expectedAnnData.third, annData.third)

        val expectedEventData = parse.genAnnotationJson()
        val eventData = parse.genJsonFromDB("annotation")
        Assert.assertEquals(expectedEventData.first.toString(), eventData.first.toString())
        Assert.assertEquals(expectedEventData.second, eventData.second)
        Assert.assertEquals(expectedEventData.third, eventData.third)
    }
}