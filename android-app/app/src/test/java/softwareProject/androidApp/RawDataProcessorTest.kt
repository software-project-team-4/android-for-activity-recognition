package softwareProject.androidApp

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import softwareProject.androidApp.internalDatabase.AccDataPoint
import softwareProject.androidApp.internalDatabase.GyrDataPoint
import softwareProject.androidApp.internalDatabaseTests.SensorDatabaseTest

class RawDataProcessorTest : SensorDatabaseTest() {

    lateinit var classify: MockEvent

    class MockEvent() : EventActivity(SensorClass()) {

        override fun initialize() {
        }
    }

    @Before
    fun setUp() {
        classify = RawDataProcessorTest.MockEvent()
    }

    @Test
    fun `matchValues return correct size array`() {
        val rawDataProcessor = RawDataProcessor(db, classify)
        val accDataPoints = (0..124 step 5).map { x ->
            (1..x * 2).map { o ->
                AccDataPoint(
                    o.toLong(),
                    o.toFloat(),
                    (o + 1).toFloat(),
                    (o + 2).toFloat()
                )
            }
        }
        val gyrdataPoints = (0..124 step 5).map { x ->
            (1..x).map { o ->
                GyrDataPoint(
                    o.toLong(),
                    o.toFloat(),
                    (o + 1).toFloat(),
                    (o + 2).toFloat()
                )
            }
        }
        for (i in 1..(accDataPoints.size - 1)) {
            val matchedValues = rawDataProcessor.matchValues(accDataPoints[i], gyrdataPoints[i])
            assertEquals((i * 2) * 5, matchedValues.size)
        }
    }

    @Test
    fun `matchValues return data contains the timestamps of the larger list`() {
        val rawDataProcessor = RawDataProcessor(db, classify)
        val accDataPoints = (0..124 step 5).map { x ->
            (1..x).map { o ->
                AccDataPoint(
                    o.toLong(),
                    o.toFloat(),
                    (o + 1).toFloat(),
                    (o + 2).toFloat()
                )
            }
        }
        val gyrdataPoints = (0..124 step 5).map { x ->
            (1..x * 2).map { o ->
                GyrDataPoint(
                    o.toLong(),
                    o.toFloat(),
                    (o + 1).toFloat(),
                    (o + 2).toFloat()
                )
            }
        }
        for (i in 1..(accDataPoints.size - 1)) {
            val matchedValues = rawDataProcessor.matchValues(accDataPoints[i], gyrdataPoints[i])
            val timestamps = matchedValues.map { v -> v.timestamp }
            assertEquals((1..((i * 5) * 2)).map { v -> v.toLong() }, timestamps)
        }
    }

    @Test
    fun `matchValues the amount of filled average values grows as the larger list grows`() {
        val rawDataProcessor = RawDataProcessor(db, classify)
        val accDataPoints =
            (1..2).map { o ->
                AccDataPoint(
                    o.toLong(),
                    o.toFloat(),
                    (o + 1).toFloat(),
                    (o + 2).toFloat()) }
        val gyrdataPoints = (0..124 step 5).map { x ->
            (1..x).map { o ->
                GyrDataPoint(
                    o.toLong(),
                    o.toFloat(),
                    (o + 1).toFloat(),
                    (o + 2).toFloat()
                )
            }
        }
        for (i in 1..(gyrdataPoints.size - 1)) {
            val matchedValues = rawDataProcessor.matchValues(accDataPoints, gyrdataPoints[i])

            assertEquals(matchedValues.count { it.acc_x == 1.5F && it.acc_y == 2.5F && it.acc_z == 3.5F }, i * 5 - 2)
        }
    }

    @Test
    fun `matchValues should return correct filled gyro data1`() {
        val rawDataProcessor = RawDataProcessor(db, classify)
        val accDataPoints = (3..9 step 2).map { x ->
            (1..x).map { o ->
                AccDataPoint(
                    o.toLong(),
                    o.toFloat(),
                    (o + 1).toFloat(),
                    (o + 2).toFloat()
                )
            }
        }
        val gyrdataPoints = (2..5 step 1).map { x ->
            (1..x).map { o ->
                GyrDataPoint(
                    o.toLong(),
                    o.toFloat(),
                    (o + 1).toFloat(),
                    (o + 2).toFloat()
                )
            }
        }
        val a = floatArrayOf(1.0F, 1.5F, 2.0F)
        val correctReturnValues = listOf(floatArrayOf(1.0F, 1.5F, 2.0F), floatArrayOf(1.0F, 1.5F, 2.0F, 2.5F, 3.0F), floatArrayOf(1.0F, 1.5F, 2.0F, 2.5F, 3.0F, 3.5F, 4.0F), floatArrayOf(1.0F, 1.5F, 2.0F, 2.5F, 3.0F, 3.5F, 4.0F, 4.5F, 5.0F))
        for (i in 0..(accDataPoints.size - 1)) {
            val matchedValues = rawDataProcessor.matchValues(accDataPoints[i], gyrdataPoints[i])
            val gyros = matchedValues.map { v -> v.gyro_x }
            assertTrue(correctReturnValues[i].all { v -> gyros.contains(v) })
        }
    }

    @Test
    fun `matchValues should return correct filled gyro data2`() {
        val rawDataProcessor = RawDataProcessor(db, classify)
        val accDataPoints = (3..12 step 3).map { x ->
            (1..x).map { o ->
                AccDataPoint(
                    o.toLong(),
                    o.toFloat(),
                    (o + 1).toFloat(),
                    (o + 2).toFloat()
                )
            }
        }
        val gyrdataPoints = (2..5 step 1).map { x ->
            (1..x).map { o ->
                GyrDataPoint(
                    o.toLong(),
                    o.toFloat(),
                    (o + 1).toFloat(),
                    (o + 2).toFloat()
                )
            }
        }
        val a = floatArrayOf(1.0F, 1.5F, 2.0F)
        val correctReturnValues = listOf(floatArrayOf(1.0F, 1.5F, 2.0F), floatArrayOf(1.0F, 1.5F, 2.0F, 2.5F, 2.5F, 3.0F), floatArrayOf(1.0F, 1.5F, 2.0F, 2.5F, 2.5F, 3.0F, 3.5F, 3.5F, 4.0F), floatArrayOf(1.0F, 1.5F, 2.0F, 2.5F, 2.5F, 3.0F, 3.5F, 3.5F, 4.0F, 4.5F, 4.5F, 5.0F))
        for (i in 0..(accDataPoints.size - 1)) {
            val matchedValues = rawDataProcessor.matchValues(accDataPoints[i], gyrdataPoints[i])
            val gyros = matchedValues.map { v -> v.gyro_x }
            assertTrue(correctReturnValues[i].all { v -> gyros.contains(v) })
        }
    }
}