package softwareProject.androidApp

import android.widget.SeekBar
import android.widget.Switch
import kotlinx.android.synthetic.main.sensor_config.*
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class SettingsActivityTest {

    private lateinit var activity: SettingsActivity
    private lateinit var seekBar: SeekBar
    private lateinit var switch: Switch

    private var initialDelay: Long = Settings.delay

    @Before
    fun setUp() {
        activity = Robolectric.setupActivity(SettingsActivity::class.java)
        seekBar = activity.seekBar
        switch = activity.switch1
    }

    @Test
    fun `after moving the slider the delay should change`() {
        activity.delayList.indices.forEach { i ->
            seekBar.progress = i
            assert(Settings.delay == activity.delayList[i].toLong())
        }
    }

    @Test
    fun `pressing the offline switch enables and disables offline mode`() {
        switch.isChecked = true
        Assert.assertEquals(true, Settings.offline)
        switch.isChecked = false
        Assert.assertEquals(false, Settings.offline)
    }

    @Test
    fun `closing the activity after changing ip changes the setting`() {
        activity.iptext.setText("123.456.789.000")
        activity.toSendingView(activity.backButton)
        assert(Settings.ipAddress == "123.456.789.000")
    }

    @After
    fun tearDown() {
        Settings.delay = initialDelay
    }
}