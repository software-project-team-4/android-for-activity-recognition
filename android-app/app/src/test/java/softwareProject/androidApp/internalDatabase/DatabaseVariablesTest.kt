package softwareProject.androidApp.internalDatabase

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Assert
import org.robolectric.RobolectricTestRunner
import softwareProject.androidApp.internalDatabaseTests.SensorDatabaseTest
import kotlin.random.Random

@RunWith(RobolectricTestRunner::class)
class DatabaseVariablesTest : SensorDatabaseTest() {

    @Test
    fun `updateVariables updates accDataPoints correctly`() {
        val count = Random.nextLong(10) + 1
        (1..count).map { i ->
            RawData(i, 0.0f, 0.0f, 0.0f, 0f, 0f, 0f)
        }.forEach { point ->
            SensorDatabase.database().insertDataPoint(point)
        }
        looper.runToEndOfTasks()
        // Set to incorrect
        DatabaseVariables.totalDataPoints = -1
        DatabaseVariables.updateVariables()
        looper.runToEndOfTasks()
        Assert.assertEquals(count, DatabaseVariables.totalDataPoints.toLong())
    }

    @Test
    fun `updateVariables updates eventDataPoints correctly`() {
        val count = Random.nextLong(10) + 1
        (1..count).map { i ->
            EventProbabilities(i, i + 1, "", "")
        }.forEach { point ->
            SensorDatabase.database().insertEventData(point)
        }
        looper.runToEndOfTasks()
        // Set to incorrect
        DatabaseVariables.totalEventPoints = -1
        DatabaseVariables.updateVariables()
        looper.runToEndOfTasks()
        Assert.assertEquals(count, DatabaseVariables.totalEventPoints.toLong())
    }

    @Test
    fun `updateVariables updates annotationDataPoints correctly`() {
        val count = Random.nextLong(10) + 1
        (1..count).map { i ->
            AnnotationDataPoint(i, i + 1, "")
        }.forEach { point ->
            SensorDatabase.database().insertAnnotationData(point)
        }
        looper.runToEndOfTasks()
        // Set to incorrect
        DatabaseVariables.totalAnnotationPoints = -1
        DatabaseVariables.updateVariables()
        looper.runToEndOfTasks()
        Assert.assertEquals(count, DatabaseVariables.totalAnnotationPoints.toLong())
    }
}