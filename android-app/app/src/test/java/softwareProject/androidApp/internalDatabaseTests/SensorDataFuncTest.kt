package softwareProject.androidApp.internalDatabaseTests

import junit.framework.Assert.assertEquals
import org.junit.Test
import softwareProject.androidApp.internalDatabase.DatabaseVariables
import softwareProject.androidApp.internalDatabase.RawData

class SensorDataFuncTest : SensorDatabaseTest() {

    @Test
    fun `Inserts and reads data`() {
        lateinit var dataPoints: List<RawData>
        lateinit var dataPoint: RawData
        lateinit var newestDataPoint: RawData
        lateinit var oldestDataPoint: RawData

        (1..10).map { i ->
            db.insertDataPoint(RawData(i.toLong(), 0F, 0F, 0F, 0F, 0F, 0F))
            looper.runToEndOfTasks()
        }

        db.getAllDataPoints { result -> dataPoints = result }
        looper.runToEndOfTasks()
        newestDataPoint = dataPoints.first()
        oldestDataPoint = dataPoints.last()
        assertEquals(10, newestDataPoint.timestamp)
        assertEquals(1, oldestDataPoint.timestamp)
        assertEquals(10, DatabaseVariables.totalDataPoints)
        db.deleteAllDataFromDb()
        looper.runToEndOfTasks()
    }

    @Test
    fun `Reads amount of data points`() {
        lateinit var dataPoints: List<RawData>
        lateinit var newestDataPoint: RawData

        (1..10).map { i ->
            db.insertDataPoint(RawData(i.toLong(), 0F, 0F, 0F, 0F, 0F, 0F))
            looper.runToEndOfTasks()
        }

        db.getAmountDataPoints(5) { result -> dataPoints = result }
        looper.runToEndOfTasks()
        newestDataPoint = dataPoints.first()
        assertEquals(5, newestDataPoint.timestamp)
        assertEquals(5, dataPoints.size)
        db.deleteAllDataFromDb()
        looper.runToEndOfTasks()
    }

    @Test
    fun `Delete before inserting if limit reached`() {
        lateinit var dataPoints: List<RawData>
        lateinit var dataPoint: RawData
        lateinit var oldestDataPoint: RawData

        DatabaseVariables.maxRowsInTable = 6

        (1..10).map { i ->
            db.insertDataPoint(RawData(i.toLong(), 0F, 0F, 0F, 0F, 0F, 0F))
            DatabaseVariables.lastEvaluatedDataPoint = i.toLong()
            looper.runToEndOfTasks()
        }
        DatabaseVariables.maxRowsInTable = 4320000
        db.getAllDataPoints { result -> dataPoints = result }
        looper.runToEndOfTasks()
        oldestDataPoint = dataPoints.last()
        assertEquals(7, oldestDataPoint.timestamp)
        assertEquals(4, DatabaseVariables.totalDataPoints)
        db.deleteAllDataFromDb()
        looper.runToEndOfTasks()
    }

    @Test
    fun `Delete data until timestamp`() {
        lateinit var dataPoint: RawData
        var timestamp = 4L

        (1..10).map { i ->
            db.insertDataPoint(RawData(i.toLong(), 0F, 0F, 0F, 0F, 0F, 0F))
            looper.runToEndOfTasks()
        }

        db.deleteDataUntil(timestamp)
        looper.runToEndOfTasks()
        assertEquals(6, DatabaseVariables.totalDataPoints)
        db.deleteAllDataFromDb()
        looper.runToEndOfTasks()
    }
}