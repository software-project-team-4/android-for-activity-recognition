package softwareProject.androidApp.internalDatabaseTests

import org.junit.Assert.assertEquals
import org.junit.Test
import softwareProject.androidApp.internalDatabase.DatabaseVariables
import softwareProject.androidApp.internalDatabase.EventProbabilities

class EventFuncTest : SensorDatabaseTest() {

    @Test
    fun should_Insert_And_Read() {
        lateinit var eventData: List<EventProbabilities>
        lateinit var eventDataPoint: EventProbabilities
        lateinit var newestEventDataPoint: EventProbabilities
        lateinit var oldestEvetDataPoint: EventProbabilities
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
        for (t in 1398902555536968242..1398902555536968342 step 10) {
            eventDataPoint = EventProbabilities(t, t + 5, "fall", "")
            db.insertEventData(eventDataPoint)
            looper.runToEndOfTasks()
        }

        db.getAllEvePoints { result -> eventData = result }
        looper.runToEndOfTasks()
        newestEventDataPoint = eventData.first()
        oldestEvetDataPoint = eventData.last()
        assertEquals(1398902555536968342, newestEventDataPoint.timestamp_start)
        assertEquals(1398902555536968242, oldestEvetDataPoint.timestamp_start)
        assertEquals(11, DatabaseVariables.totalEventPoints)
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
    }

    @Test
    fun should_Read_Amount_Of_Points() {
        lateinit var eventData: List<EventProbabilities>
        lateinit var eventPoint: EventProbabilities
        lateinit var newestEventPoint: EventProbabilities

        for (t in 1398902555536968242..1398902555536968342 step 10) {
            eventPoint = EventProbabilities(t, t + 5, "fall", "")
            db.insertEventData(eventPoint)
            looper.runToEndOfTasks()
        }

        db.getAmountEvePoints(5) { result -> eventData = result }
        looper.runToEndOfTasks()
        newestEventPoint = eventData.first()
        assertEquals(1398902555536968282, newestEventPoint.timestamp_start)
        assertEquals(5, eventData.size)
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
    }

    @Test
    fun should_Delete_Data_Points_Until_Timestamp() {
        lateinit var eventDataPoint: EventProbabilities
        var timestamp = 1398902555536968292

        for (t in 1398902555536968242..1398902555536968342 step 10) {
            eventDataPoint = EventProbabilities(t, t + 5, "fall", "")
            db.insertEventData(eventDataPoint)
            looper.runToEndOfTasks()
        }

        db.deleteEventDataUntil(timestamp)
        looper.runToEndOfTasks()
        assertEquals(5, DatabaseVariables.totalEventPoints)
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
    }
}