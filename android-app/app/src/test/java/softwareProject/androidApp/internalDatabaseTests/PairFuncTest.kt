package softwareProject.androidApp.internalDatabaseTests

import org.junit.Assert
import org.junit.Test
import softwareProject.androidApp.internalDatabase.AnnotationAndEvent
import softwareProject.androidApp.internalDatabase.DatabaseVariables

class PairFuncTest : SensorDatabaseTest() {
    lateinit var pair: AnnotationAndEvent
    lateinit var oldestPair: AnnotationAndEvent
    lateinit var latestPair: AnnotationAndEvent

    lateinit var pairData: List<AnnotationAndEvent>

    @Test
    fun `should insert and read`() {
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
        for (t in 1398902555536968242..1398902555536968342 step 10) {
            pair = AnnotationAndEvent(t, "Walking", "Walking")
            db.addEventAnnotationPair(pair)
            looper.runToEndOfTasks()
        }

        db.getEventAnnotationPairs(DatabaseVariables.totalPairPoints) { result -> pairData = result }
        looper.runToEndOfTasks()

        oldestPair = pairData.last()
        latestPair = pairData.first()
        Assert.assertEquals(1398902555536968342, latestPair.timestamp)
        Assert.assertEquals(1398902555536968242, oldestPair.timestamp)
        Assert.assertEquals(11, DatabaseVariables.totalPairPoints)
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
    }

    @Test
    fun `should delete pairs until amount`() {
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
        for (t in 1398902555536968242..1398902555536968342 step 10) {
            pair = AnnotationAndEvent(t, "Walking", "Walking")
            db.addEventAnnotationPair(pair)
            looper.runToEndOfTasks()
        }
        db.deleteAmountPairPoints(5)
        looper.runToEndOfTasks()
        db.getEventAnnotationPairs(DatabaseVariables.totalPairPoints) { result -> pairData = result }
        looper.runToEndOfTasks()
        oldestPair = pairData.last()
        latestPair = pairData.first()
        Assert.assertEquals(1398902555536968342, latestPair.timestamp)
        Assert.assertEquals(1398902555536968292, oldestPair.timestamp)
        Assert.assertEquals(6, DatabaseVariables.totalPairPoints)
    }
}