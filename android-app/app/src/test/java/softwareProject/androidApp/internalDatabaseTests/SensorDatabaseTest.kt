package softwareProject.androidApp.internalDatabaseTests

import androidx.test.core.app.ApplicationProvider
import org.junit.Before
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows
import org.robolectric.shadows.ShadowLooper
import softwareProject.androidApp.internalDatabase.DbWorkerThread
import softwareProject.androidApp.internalDatabase.SensorDatabase

@RunWith(RobolectricTestRunner::class)
abstract class SensorDatabaseTest {

    private lateinit var worker: DbWorkerThread
    lateinit var looper: ShadowLooper
    lateinit var db: SensorDatabase

    @Before
    fun setup() {
        SensorDatabase.init(ApplicationProvider.getApplicationContext(), true)
        db = SensorDatabase.database()
        worker = SensorDatabase.workerThread()
        looper = Shadows.shadowOf(worker.looper)
    }
}