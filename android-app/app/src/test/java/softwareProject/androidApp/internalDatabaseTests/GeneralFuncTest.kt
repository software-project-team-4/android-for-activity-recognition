package softwareProject.androidApp.internalDatabaseTests

import junit.framework.Assert.assertEquals
import org.junit.Test
import softwareProject.androidApp.internalDatabase.AnnotationDataPoint
import softwareProject.androidApp.internalDatabase.EventProbabilities
import softwareProject.androidApp.internalDatabase.RawData
import softwareProject.androidApp.internalDatabase.DatabaseVariables

class GeneralFuncTest : SensorDatabaseTest() {

    @Test
    fun should_Delete_All_Data() {
        lateinit var dataPoint: RawData
        lateinit var annotationDataPoint: AnnotationDataPoint
        lateinit var eventDataPoint: EventProbabilities

        for (t in 1398902555536968242..1398902555536968252) {
            dataPoint = RawData(t, 123.25F, 54.34F, 23.41F, 123.25F, 54.34F, 23.41F)
            db.insertDataPoint(dataPoint)
            looper.runToEndOfTasks()
        }

        for (t in 1398902555536968242..1398902555536968342 step 10) {
            eventDataPoint = EventProbabilities(t, t + 5, "fall", "")
            annotationDataPoint = AnnotationDataPoint(t, t + 5, "fall")
            db.insertEventData(eventDataPoint)
            db.insertAnnotationData(annotationDataPoint)
            looper.runToEndOfTasks()
        }

        db.deleteDataFromDb()
        looper.runToEndOfTasks()
        assertEquals(0, DatabaseVariables.totalDataPoints)
        assertEquals(0, DatabaseVariables.totalEventPoints)
        assertEquals(0, DatabaseVariables.totalAnnotationPoints)
    }
}