package softwareProject.androidApp.internalDatabaseTests

import org.junit.Assert.assertEquals
import org.junit.Test
import softwareProject.androidApp.internalDatabase.AnnotationDataPoint
import softwareProject.androidApp.internalDatabase.DatabaseVariables

class AnnotationFuncTest : SensorDatabaseTest() {

    @Test
    fun `should insert and read`() {
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
        lateinit var annotationData: List<AnnotationDataPoint>
        lateinit var annotationDataPoint: AnnotationDataPoint
        lateinit var newestAnnotationDataPoint: AnnotationDataPoint
        lateinit var oldestAnnotatinoDataPoint: AnnotationDataPoint

        for (t in 1398902555536968242..1398902555536968342 step 10) {
            annotationDataPoint = AnnotationDataPoint(t, t + 5, "fall")
            db.insertAnnotationData(annotationDataPoint)
            looper.runToEndOfTasks()
        }

        db.getAllAnnPoints { result -> annotationData = result }
        looper.runToEndOfTasks()
        newestAnnotationDataPoint = annotationData.first()
        oldestAnnotatinoDataPoint = annotationData.last()
        assertEquals(1398902555536968342, newestAnnotationDataPoint.timestamp_start)
        assertEquals(1398902555536968242, oldestAnnotatinoDataPoint.timestamp_start)
        assertEquals(11, DatabaseVariables.totalAnnotationPoints)
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
    }

    @Test
    fun `should read amount of points`() {
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
        lateinit var annotationData: List<AnnotationDataPoint>
        lateinit var annotationPoint: AnnotationDataPoint
        lateinit var newestAnnotationPoint: AnnotationDataPoint

        for (t in 1398902555536968242..1398902555536968342 step 10) {
            annotationPoint = AnnotationDataPoint(t, t + 5, "fall")
            db.insertAnnotationData(annotationPoint)
            looper.runToEndOfTasks()
        }

        db.getAmountAnnPoints(5) { result -> annotationData = result }
        looper.runToEndOfTasks()
        newestAnnotationPoint = annotationData.first()
        assertEquals(1398902555536968342, newestAnnotationPoint.timestamp_start)
        assertEquals(5, annotationData.size)
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
    }

    @Test
    fun `should delete data points until timestamp`() {
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
        lateinit var annotationDataPoint: AnnotationDataPoint
        var timestamp = 1398902555536968292

        for (t in 1398902555536968242..1398902555536968342 step 10) {
            annotationDataPoint = AnnotationDataPoint(t, t + 5, "fall")
            db.insertAnnotationData(annotationDataPoint)
            looper.runToEndOfTasks()
        }

        db.deleteAnnotationDataUntil(timestamp)
        looper.runToEndOfTasks()
        assertEquals(5, DatabaseVariables.totalAnnotationPoints)
        db.deleteDataFromDb()
        looper.runToEndOfTasks()
    }
}