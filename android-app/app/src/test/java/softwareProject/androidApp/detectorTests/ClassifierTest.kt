package softwareProject.androidApp.detectorTests

import org.junit.Assert
import org.junit.Before
import org.junit.Test
import softwareProject.androidApp.EventActivity
import softwareProject.androidApp.SensorClass
import softwareProject.androidApp.internalDatabase.DatabaseVariables
import softwareProject.androidApp.internalDatabase.RawData
import softwareProject.androidApp.internalDatabaseTests.SensorDatabaseTest

class ClassifierTest() : SensorDatabaseTest() {
    lateinit var classify: MockEvent

    class MockEvent() : EventActivity(SensorClass()) {

        override fun initialize() {
        }

        override fun runModel(data: List<RawData>) {
            returnPoint = "classify"
        }
    }

    @Before
    fun setUp() {
        classify = MockEvent()
    }

    /* @Test
     fun `should return correct return point when no data exists`() {
         DatabaseVariables.lastEvaluatedDataPoint = 0L
         db.deleteDataFromDb()
         classify.scheduled = false
         classify.classifyData()
         looper.runToEndOfTasks()
         Assert.assertEquals("data", classify.returnPoint)
         Assert.assertEquals(true, classify.scheduled)
     }

     @Test
     fun `should run until the end when data is available`() {
         DatabaseVariables.lastEvaluatedDataPoint = 0L
         classify.scheduled = false
         db.deleteDataFromDb()
         looper.runToEndOfTasks()
         for (i in 1L..252L) {
             val dataPoint = RawData(i, 12F, 45F, 8F, 12F, 45F, 8F)
             db.insertDataPoint(dataPoint)
             looper.runToEndOfTasks()
         }
         classify.classifyData()
         looper.runToEndOfTasks()
         Assert.assertEquals("classify", classify.returnPoint)
         Assert.assertEquals(true, classify.scheduled)
     }
 */
    @Test
    fun `should update evaluated timestamps correctly`() {
        DatabaseVariables.lastEvaluatedDataPoint = 0
        for (i in 1L..252L) {
            val dataPoint = RawData(i, 12F, 45F, 8F, 12F, 45F, 8F)
            db.insertDataPoint(dataPoint)
            looper.runToEndOfTasks()
        }
        var data: List<RawData> = emptyList()
        db.getAmountEvalDataPoints(250, DatabaseVariables.lastEvaluatedDataPoint) { result ->
            data = result
        }
        looper.runToEndOfTasks()
        classify.convertToFloat(data)
        val lastEvalPoint = DatabaseVariables.lastEvaluatedDataPoint
        val asd = classify.timestamp_start.toString()
        println("first: $asd")
        println("last: $lastEvalPoint")

        Assert.assertEquals(1L, classify.timestamp_start)
        Assert.assertEquals(250L, lastEvalPoint)
    }
}