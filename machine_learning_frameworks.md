# Machine Learning Frameworks that run on mobile


## Introduction
The main goal of our project is to look for possible ways to replace or enhance the standard way of running machine learning algorithms server side to run them directly on your mobile. For our project it would be unpractical to make the machine learning frameworks by ourselves so we have decided to search for possible frameworks that run on mobile.

Here we have a picture of KDnuggets survey on the question *"What Analytics, Big Data, Data Science, Machine Learning software you used in the past 12 months for a real project?"*.



<img src="https://cdn-images-1.medium.com/max/960/1*6b2Tm0oFr99zKaxkr4g52w.png">


Our limitation to these frameworks was that the framework had to be compatible to run on android. With this limitation in mind we narrowed our frameworks to 3 potential candidates.


## Possible frameworks
***

<img src="https://lh3.googleusercontent.com/MPePZnl0pGfNpQV8Y6iVQhAwM8mxP-DRJyaC5OAaLGMoM0hrALg76z7WaM-3WYcclwoc7Kd18xOEX9X0-xc22em76tHGzg=s688" width="200" align="left">

<br  />
<br  />
<br  />
<br  />
<br  />



## 1.Tensorflow/Tensorflow lite by Google
TensorFlow Lite is TensorFlow’s lightweight solution for mobile and embedded devices. It enables on-device machine learning inference with low latency and a small binary size. TensorFlow Lite also supports hardware acceleration with the Android Neural Networks API.[1]

### Suitability for this project

Before we started this project, tensorflow got the most endorsement on our team. People who had previous experience on machine learning had already heard of it or even used it. With the release of tensorflow lite on 2017 the deployment of tensorflow made models to mobile devices (especially for android) has come fairly easy. Tensorflow has great support especially for recurrent and convolutional neural networks, which our model consists of. <br />
 Tensorflow seemed to be the obvious choice but after searching the web and comparing frameworks the decision did not come easy.


### <u>Pros:</u>

- Most used framework for deep learning

- All the models that we currently find in HAR use tensorflow

- Easy to integrate between server side and android

- Fast and supports recursive neural networks

### <u>Cons:</u>

- None at the moment

***


<img src="https://caffe2.ai/static/logo.svg" width="100" align="left"/>
<br  />
<br  />
<br  />
<br  />
<br  />
<br  />


## 2. Caffe2 by Facebook
Caffe2 is a deep learning framework that provides an easy and straightforward way for you to experiment with deep learning and leverage community contributions of new models and algorithms. You can bring your creations to scale using the power of GPUs in the cloud or to the masses on mobile with Caffe2’s cross-platform libraries.[2]

### SUITABILITY FOR THIS PROJECT

Caffe2 is another widely used machine learning framework made by facebook. Big advantages of Caffe2 are that it integrates with android studio for mobile development. Also caffe2 offers a "model zoo" where you can find pre-trained models ready for mobile deployment. Sadly model zoo doesn't offer a pre-trained human activity recognition model so we won't benefit from it.



### <u>Pros:</u>
- Really fast running times on some models (i.e normal Artificial Neural Networks (ANN's))





### <u>Cons:</u>
- Not as widely used as tensorflow

- Caffe2's model zoo has only few HAR models



***


<img src="https://cdn-images-1.medium.com/max/1600/1*u2t2N3lu8sH1CSsSrP_UyQ.png" align="left" width=300>
<br  />
<br  />
<br  />
<br  />
<br  />
<br  />

## 3. Deeplearning4j
Deeplearning4j is written in Java and is compatible with any JVM language, such as Scala, Clojure or Kotlin. The underlying computations are written in C, C++ and Cuda. Keras will serve as the Python API. [3]

### SUITABILITY FOR THIS PROJECT

Deeplearning4j would be the best framework for our model if we would've chosen to create our model all by ourselves. Here the variety of compatible languages would've made a big difference especially since kotlin and scala are used as our front- and backend languages. The lack of existing projects made us simply prefer tensorflow over deeplearning4j.


### <u>Pros:</u>
- Documentation made really simple

- All around a good framework

- Compatible with any JVM language

### <u>Cons:</u>
- Not as common as other frameworks



***

## Conclusion

We decided to go with tensorflow simply because we found that all of the existing models used it mainly as their go-to framework. Tensorflow has a great documentation and is the most suitable for our project. Deployment of the pre-trained models to android with tensorflow has been made relatively easy.



## References
Picture 1: https://cdn-images-1.medium.com/max/960/1*6b2Tm0oFr99zKaxkr4g52w.png


Picture 2: https://lh3.googleusercontent.com/MPePZnl0pGfNpQV8Y6iVQhAwM8mxP-DRJyaC5OAaLGMoM0hrALg76z7WaM-3WYcclwoc7Kd18xOEX9X0-xc22em76tHGzg=s688

Picture 3: https://caffe2.ai/static/logo.svg

Picture 4: https://cdn-images-1.medium.com/max/1600/1*u2t2N3lu8sH1CSsSrP_UyQ.png


[1]https://www.tensorflow.org/lite/overview

[2]https://caffe2.ai/docs/caffe-migration.html

[3]https://deeplearning4j.org/
