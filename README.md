# Machine Learning Human Activity Recognition on Android

## Description

The aim of this project was to study the feasibility of collecting data from sensors on mobile devices and running a machine learning model, on-device, to recognize which activity the user is currently performing. Due to the nature of the task, with temporal dependencies and many variations for any given activity, it is impractical to hand engineer an algorithm to achieve useful results. Instead, machine learning can be used to find and learn the patterns in the data and map a time window of data to an activity classification. Especially deep learning methods are well suited for such a task.

Recent progress in mobile chips and tensor processing units along with maturing machine learning frameworks has made running such a model on the device itself increasingly more viable.

We built an Android app for running on-device inference on a deep convolutional neural network. The app also allows the user to collect and annotate data. For ingesting the data produced by the app, we also built a backend server which processes the raw data, predictions and annotations and inserts them into a database. A Grafana dashboard can then be used to visualize this data.

## The App

The Android app collects data from the accelerometer and gyroscope and stores it in an internal database on the device. The data is then read from the database, processed into suitable features which are then fed to the machine learning model. The model produces activity classifications which are sent to the backend server along with the raw sensor data. Once the data has successfully been sent to the server it is deleted from the database. As the model runs on the device itself, no connectivity is needed for performing the recognition.

The UI consists of buttons for each possible activity the model is trained to recognize and next to which is the corresponding probability predicted by the model. The __DELETE__ button allows the user to clear the database, the __STOP ANNOTATION__ stops the current annotation and __SIZE__ shows the amount of stored data points in the database.

<img src="./Images/UI_cropped.jpg"  width="300" height="500" border="1">

## The Backend

The backend receives three different types of data from the app:
1. Raw sensor data from the phone's gyroscope and accelerometer.
2. Event data from the machine learning model's predictions.
3. Annotation data when the user annotates their doings.

The backend was built with Play Framework in Scala. The main function of it is to receive the data sent by the mobile app, validate the JSON and read the values into variables. It has separate controllers for receiving the raw data, annotations and the events produced by the model on the device. The values are then inserted into the database. A second model can also be run on the server and TensorFlow Serving can be used to run it as a separate service. After insertion, the raw data is processed into features which can be sent to the model.

<img src="./Images/grafana.png">

**The Repository**

[Backend for Human Activity Recognition on Mobile](https://gitlab.com/software-project-team-4/backend-for-activity-recognition/)

## The Machine Learning Model

The model which runs on the device is a convolutional neural network with five layers and it uses a five second time window with sensor readings sampled at 50 times per second. The input vector is therefore 250*6 in size as there are two sensors with readings from three axes each. Max pooling and dropout is applied between layers. Two fully-connected layers are added on top. Currently TensorFlow Lite doesn't support the conversion of recurrent units and therefore these couldn't be utilized in the mobile model. The model which runs on the server has additionally a bi-directional LSTM layer added.

**The Repository**

[ML for Human Activity Recognition on Mobile](https://gitlab.com/software-project-team-4/ml-for-activity-recognition)

## Additional Info

#### Short Preview of the App Files

Java folder is separated to two folders: The folder that contains tests and the folder which contains all the Kotlin code.
- __Auth folder__: Contains the google authentication code
- __Detector folder__: Contains fall detector implementation without ML
- __internalDatabase__: Contains all of the code related to building the apps database
- __SensorClass.kt__: The main activity of the app which launches the main_screen (UI) from the  res folder.
- __DataSending.kt__: Scheduling the data sending patches
- __EventActivity.kt__: Contains the code for running the model
- __LogActivity.kt__: The activity for displaying past events and annotations
- __ParseJson.kt__: Deals with parsing the data into json format for sending
- __RawDataProcessor.kt__: Matching of the gyroscope and accelerometer readings


#### Setting Up

1. Clone the git repository
2. Import the project into Android Studio as an Android Gradle project
3. Enable USB debugging on your device
4. Run the app from Android Studio

#### Changing the ML Model

1. First convert the TensorFlow model to a tflite file and transfer it to the assets folder of the project.
2. Locate the Settings.kt folder in the project and overwrite path2 variable to the name of your new model
3. Go to SensorClass.kt and on the onCreate function change the eventActivity = eventActivity(Settings.path2 , <*HERE PUT THE SIZE OF MODELS OUTPUT*>)
4. You can now tweak the fillCurrentValues function to represent the name of your outputs. The changeProbs function is also worth to checkout that it works correctly.

#### The Database Schema

<img src="./Images/database_schema.png">

#### Architecture Diagram

<img src="./Images/backend_architecture.png">
