# Android sensors

In Android devices there are a total of 11 different types of sensors, which existance depend on the phone in hand. If the sensors are followed they report their values when ever the values have changed(see onSensorChanged function). The frequency of these reports depend on the given sampling period. The return type for all of the sensor events is float[].

**Sensor availability:** While sensor availability varies from device to device, it can also vary between Android versions. This is because the Android sensors have been introduced over the course of several platform releases. 

## Monitoring sensor events

**Sensor reports a new value**

In this case the system invokes the onSensorChanged() method, providing you with a SensorEvent object. A SensorEvent object contains information about the new sensor data, including: the accuracy of the data, the sensor that generated the data, the timestamp at which the data was generated, and the new data values that the sensor recorded.

**Sensor's accuracy changed**

In this case the system invokes the onAccuracyChanged() method, providing you with a reference to the Sensor object that changed and the new accuracy of the sensor.

## Base Sensors 

Base sensors are named after the physical sensor they represent and get data from only one sensor. The ouput of this sensor is not the same as the physical sensoräs, because of corrections made to the measures.

### Accelerometer

**Description:** Reports the speed of the device in metres per second squared(m/s^2) including the force of gravity

**Values:**<br />
values[0]: Acceleration minus Gx on the x-axis <br /> 
values[1]: Acceleration minus Gy on the y-axis <br /> 
values[2]: Acceleration minus Gz on the z-axis

### Ambient temperature

**Description:** Reports the ambient air temperature in celcius(C).

**Values:**<br />
Value[0]: ambient (room) temperature in degree Celsius.

### Gyroscope

**Description:** Rotation of the device to some standard point of reference. Rotation is positive in the counterclockwise direction. The measured values are given in radians per second (rad/s).

**Values:**<br />
values[0]: Angular speed around the x-axis <br /> 
values[1]: Angular speed around the y-axis <br /> 
values[2]: Angular speed around the z-axis

### Light

**Description:** Reports current illumination in lux(lx)

**Values:**<br />
values[0]: Ambient light level in SI lux units

### Magnetic field sensor

**Description:** Reports the ambient magnetic field in micro-Tesla(uT), as measured along the 3 sensor axes. 

**Values:**<br />
values[0]: Magnetic field for the x-axis <br /> 
values[1]: Magnetic field for the y-axis <br /> 
values[2]: Magnetic field for the z-axis

### Pressure

**Description:** Reports the ambiet air pressure in hectopascals(hpa) .

**Values:**<br />
values[0]: Atmospheric pressure in hPa (millibar)

### Proximity

**Description:** Reports distance from the view screen to the closest visible surface in centimeters(cm)

**Values:**<br />
values[0]: Proximity sensor distance measured in centimeters


### Relative humidity

**Description:** returns air humidity in percent

**Values:**<br />
values[0]: Relative ambient air humidity in percent

## Composite sensor types

On top of the base sensors android provices also composite sensors, which process and/or fuse data from on one or several base sensors.

### Linear acceleration

**Description:** A linear acceleration sensor reports the linear acceleration of the device in the sensor frame, not including gravity.

**Underlying physical sensors:** Accelerometer and (if present) Gyroscope (or magnetometer if gyroscope not present)



**Values:** acceleration = gravity + linear-acceleration <br />
values[0]: Acceleration plus Gx on the x-axis <br />
values[1]: Acceleration plus Gy on the y-axis <br />
values[2]: Acceleration plus Gz on the z-axis <br />

### Significant motion

**Description:** A significant motion detector triggers when the detecting a “significant motion”: a motion that might lead to a change in the user location. I.e walking, biking and moving with a car

**Underlying physical sensors:** Accelerometer (or another as long as low power)

**Values:**<br />
values[0]: 1

### Step detector

**Description:** A step detector generates an event each time a step is taken by the user.

**Underlying physical sensors:** Accelerometer (+ possibly others as long as low power)

**Values:**<br />
values[0]: 1

### Step counter

**Description:** A step counter reports the number of steps taken by the user since the last reboot while activated.

**Underlying physical sensors:** Accelerometer (+ possibly others as long as low power)

**Values:**<br />
values[0]: ?

### Tilt detector

**Description:** A tilt detector generates an event each time a tilt event is detected. 

**Underlying physical sensors:** Accelerometer (+ possibly others as long as low power)

**Values:**<br />
values[0]: 1

### Rotation vector

**Description:** A rotation vector sensor reports the orientation of the device relative to the East-North-Up coordinates frame. It is usually obtained by integration of accelerometer, gyroscope, and magnetometer readings. The East-North-Up coordinate system is defined as a direct orthonormal basis

**Underlying physical sensors:** Accelerometer, Magnetometer, and Gyroscope

**Values:**<br />
values[0]: x&ast;sin(θ/2)<br />
values[1]: y&ast;sin(θ/2)<br />
values[2]: z&ast;sin(θ/2)<br />
values[3]: cos(θ/2)<br />
values[4]: estimated heading Accuracy (in radians) (-1 if unavailable)<br />

### Game rotation vector

**Description:** A game rotation vector sensor is similar to a rotation vector sensor but not using the geomagnetic field. Therefore the Y axis doesn't point north but instead to some other reference. That reference is allowed to drift by the same order of magnitude as the gyroscope drifts around the Z axis.

**Underlying physical sensors:** Accelerometer and Gyroscope (no Magnetometer)

**Values:**<br />
values[0]: x&ast;sin(θ/2)<br />
values[1]: y&ast;sin(θ/2)<br />
values[2]: z&ast;sin(θ/2)<br />
values[3]: cos(θ/2)<br />
values[4]: estimated heading Accuracy (in radians) (-1 if unavailable)<br />

### Gravity

**Description:** A gravity sensor reports the direction and magnitude of gravity in the device's coordinates.

When the device is at rest, the output of the gravity sensor should be identical to that of the accelerometer. On Earth, the magnitude is around 9.8 m/s^2.

Underlying physical sensors: Accelerometer and (if present) Gyroscope (or magnetometer if gyroscope not present)

**Values:** A three dimensional vector indicating the direction and magnitude of gravity. Units are m/s^2. <br />
values[0]: X m/s^2. <br />
values[1]: Y m/s^2. <br />
values[2]: Z m/s^2. <br />

### Geomagnetic rotation vector

**Description:** A geomagnetic rotation vector is similar to a rotation vector sensor but using a magnetometer and no gyroscope.

**Underlying physical sensors:** Accelerometer and Magnetometer (no Gyroscope)

**Values:**<br />
values[0]: x&ast;sin(θ/2)<br />
values[1]: y&ast;sin(θ/2)<br />
values[2]: z&ast;sin(θ/2)<br />
values[3]: cos(θ/2)<br />
values[4]: estimated heading Accuracy (in radians) (-1 if unavailable)<br />

### Wake up gesture

**Description:** A wake up gesture sensor enables waking up the device based on a device specific motion. When this sensor triggers, the device behaves as if the power button was pressed, turning the screen on. This behavior (turning on the screen when this sensor triggers) might be deactivated by the user in the device settings. Changes in settings do not impact the behavior of the sensor: only whether the framework turns the screen on when it triggers. The actual gesture to be detected is not specified, and can be chosen by the manufacturer of the device.

**Underlying physical sensors:** Undefined (anything low power)

**Values:**<br />
values[0] = 1

### Pick up gesture

**Description:** A pick-up gesture sensor triggers when the device is picked up regardless of wherever it was before (desk, pocket, bag).

**Underlying physical sensors:** Undefined (anything low power)

**Values:**<br />
values[0] = 1

### Glance gesture

**Description:** A glance gesture sensor enables briefly turning the screen on to enable the user to glance content on screen based on a specific motion. When this sensor triggers, the device will turn the screen on momentarily to allow the user to glance notifications or other content while the device remains locked in a non-interactive state (dozing), then the screen will turn off again. This behavior (briefly turning on the screen when this sensor triggers) might be deactivated by the user in the device settings. Changes in settings do not impact the behavior of the sensor: only whether the framework briefly turns the screen on when it triggers. The actual gesture to be detected is not specified, and can be chosen by the manufacturer of the device.

**Underlying physical sensors:** Undefined (anything low power)

**Values:**<br />
values[0] = 1



